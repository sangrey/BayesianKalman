//
// Created by sangrey on 2/17/16.
//

#ifndef KALMAN_FILTER_H
#define KALMAN_FILTER_H

#include <algorithm>
#include <armadillo>
#include <tuple>
#include <stdexcept>
#include <cmath>
#include <assert.h>
#include <type_traits>
#include <iostream>
#include <exception>
#include <cassert>
#include <csignal>
#include <numpy/npy_common.h>
#include "arma_wrapper.h"

namespace kal {

using arma::Mat;
using arma::Col;
using arma::Cube;
using arma::randn;
using arma::pinv;
using arma::kron;
using std::size_t;


using npint = npy_int;
using npdouble = npy_double;
using npulong = npy_ulong;
using intcube = arma::Cube<npint>;
using intmat = arma::Mat<npint>;
using dmat = arma::Mat<npdouble>;
using dvec = arma::Col<npdouble>;
using drow = arma::Row<npdouble>;
using dcube = arma::Cube<npdouble>;


static npdouble pi = arma::datum::pi; 
static npdouble log2pi = std::log(2 * pi);


/*
 * This function solves takes the lower cholesky factor of a matrix (A) and solves the system A \ B. 
 * It is inspired by to the cho_solve function from scipy.
 */
inline
dmat cho_solve(const dmat &lroot, const dmat &mat2) {
    return arma::solve(arma::trimatu(lroot.t()), arma::solve(arma::trimatl(lroot), mat2));
}


/*
 * This function simply averages the matrix with its tranpose.
 */
inline
dmat make_symmetric(const dmat &matrix) {
    return .5 * (matrix + matrix.t());
}


inline
dmat make_symmetric(const dmat &matrix1, const dmat matrix2) {
    return .5 * (matrix1 + matrix2.t());
}


/*
 * Extracts the relevant matrix for the given time.  This allows one to write code that will handle both 
 * time-varying matrices, which are represented as cubes, and simple matrices. This specific function does not 
 * do anyting at all, and I thing should be optimized away entirely.
 */
template<typename num>
const Mat<num>& extract_matrix(const Mat<num> &param, __attribute__((unused))  npulong time) {
    return param;
}

template<typename num>
Mat<num>& extract_mutable_matrix(Mat<num>& param, __attribute__((unused)) npulong time) { 

    return param;

}

template<typename num>
Mat<num>& extract_mutable_matrix(arma::Cube<num>& param, npulong time) { 

    return param.slice(time);
}


/*
 * A const alais for .slice().
 */
    template<typename num>
    inline const Mat<num> extract_matrix(const arma::Cube<num> &param, npulong time) {
        return  param.slice(time); 
    }

/*
 * An const alias for .row().
 */
    template<typename num>
    inline const arma::Row<num> extract_row(const Mat<num> &param, npulong time) {
        return param.row(time);
    }


/*
 * Extracts the relevant vector for a matrix of time-varying vectors.
 */
// I want the function to have thes ame signature as the matrix version.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

    template<typename num>
    inline const arma::Row<num> &extract_row(const arma::Row<num> &param, npulong time) {
        return param;
    }


    template<typename num>
    inline const arma::Row<num> extract_row(const arma::Col<num>& param, npulong time) {
        return param.t();
    }

#pragma GCC diagnostic pop


/*
 * Calculates the lower triangular root of a matrix.  If the matrix is positive-definite. it uses the cholesky
 * decomposition since that method is very fast and numerically stable. However, it uses the SVD decomposition 
 * to handle rank-deficient matrices.  To get a triangular root from the SVD decomposition, you need to use the 
 * QR factorization after the SVD decomposition, and so that is done as well. 
 */
inline dmat lt_root_mat(const dmat& psd_mat) {

    dmat root_mat(arma::size(psd_mat));
    bool factored_mat = arma::chol(root_mat, psd_mat, "lower");

    if (!factored_mat) {
        dmat u_mat(arma::size(psd_mat));
        dmat v_mat(arma::size(psd_mat));
        dvec singular_vals(psd_mat.n_rows);
        dmat q_mat(arma::size(psd_mat));

        factored_mat = arma::svd(u_mat, singular_vals, v_mat, make_symmetric(psd_mat));
        if (!factored_mat) {
            throw std::runtime_error("The matrix could not be SVD decomposed.");
         }

        dmat right_root = arma::diagmat(arma::sqrt(singular_vals)) * make_symmetric(u_mat, v_mat);
        factored_mat = arma::qr(q_mat, root_mat, right_root);

        /* We want the entries on the diagonal to be positive as the are in the Cholesky decomposition. */
        if (root_mat(0) < 0) {root_mat = -1 * root_mat;}
        if (!factored_mat) {
            throw std::runtime_error("We could not QR decompose the matrix.");
         }

        /* We want the left root. */ 
        arma::inplace_trans(root_mat); 
     }

    return root_mat;
}


/*
 * Simulates a vector autoregression. It assumes that the innovations are i.i.d normal and simply iterates
 * forward.
 */
template<typename T1, typename T2, typename T3>
arma::Mat<T1> simulate_var(const Mat<T1> &var_coeff, const Mat<T1> &innov_var, size_t time_dim,
                           const T2 &initial_state, const T3 &mean) {

    arma::arma_rng::set_seed_random();
    npulong data_dim = var_coeff.n_cols;
    arma::Mat<T1> root = lt_root_mat(innov_var);
    arma::Mat<T1> states(data_dim, time_dim + 1);
        
    states.col(0) = initial_state;
    for (npulong t = 1; t < time_dim + 1; ++t) {
        states.col(t) = mean + var_coeff * states.col(t - 1) + root * arma::randn < arma::Col<T1>>(data_dim);
     }

    return states.tail_cols(time_dim).t();

}

/* 
 * This function computes the initial state covariance matrix. If stationary is set to true, it tries to draw 
 * compute the true unconditional covariance matrix. If that does not work, it reverts to the non-stationary case. 
 * The only reason, we allow stationary to be set equal to true in the non-stationary case is for testing 
 * purposes. In general, that does not make sense. We just revert to using the first transition and innovation 
 * matrices. 
 */
inline
dmat compute_initial_cov(const dmat& trans, const dmat& state_var, bool stationary) { 

    aw::assert_same_size(trans, state_var);
    aw::assert_is_finite(trans, state_var);  
    aw::assert_square(trans, state_var);  

    dmat predictive_state_cov; 
    auto state_dim = trans.n_rows;
    auto state_dim_sq = trans.n_elem; 

    if (! stationary) {
        predictive_state_cov = state_var; 
    } else { 

        try {
            dmat weight_mat = arma::eye(state_dim_sq, state_dim_sq) - arma::kron(trans, trans).t();
            predictive_state_cov = arma::reshape(arma::solve(weight_mat, arma::vectorise(state_var)),
                                                 state_dim, state_dim);
    
         } catch (std::runtime_error) {
            predictive_state_cov  = compute_initial_cov(trans, state_var, false); 
         }
    }

    return predictive_state_cov; 


}

/* 
 * This function tries to compute the initial mean. If stationary is true, it computes the unconditional mean.
 * If that fails or sttionary is false, it reverts to just returning the state_mean. That is it assumes the 
 * previous iteration (0th) equaled zero. This is the same type of approximation as used above. 
 */ 
inline
dvec compute_initial_mean(const dmat& trans, const dvec& mean, bool stationary) {

    aw::assert_same_n_rows(trans, mean);
    aw::assert_square(trans); 
    aw::assert_is_finite(trans, mean); 

    auto state_dim = trans.n_rows;
    dmat predictive_mean;
    
    if (! stationary) {
        predictive_mean = mean;  

    } else { 

        try {

            dmat inverse_weight_mat = arma::eye(state_dim, state_dim) - trans.t(); 
            predictive_mean =  arma::solve(inverse_weight_mat, mean); 

        } catch (std::runtime_error) {
            predictive_mean = compute_initial_mean(trans, mean, false); 
        }

    }

    return predictive_mean; 
}

/* 
 * Does the size reducing transformation described in Jungbacker & Koopman 2014. 
*/
auto transform_data(const dmat& data,const dmat& innov_cov, const dvec& mean, const dmat& loadings) { 

    dmat col_space_span = arma::orth(loadings);
    npulong init_state_dim = innov_cov.n_cols;
    dmat innov_precn = cho_solve(lt_root_mat(innov_cov), arma::eye(init_state_dim,init_state_dim));     
    dmat transformation_mat = col_space_span.t() * innov_precn;  
        
    dmat transformed_cov = transformation_mat * innov_precn * transformation_mat.t(); 
    dmat transformed_data = data * transformation_mat.t();
    drow transformed_mean = transformation_mat * mean; 
    dmat transformed_loadings = transformation_mat * loadings; 

    return std::make_tuple(transformed_data, transformed_cov, transformed_mean, transformed_loadings);
}

/*
 * In the future this will call the matrix function to deal with time-varying system matrices, but right now
 * we just return the args unchanged. 
 */
auto transform_data(const dmat& data, const dcube& innov_cov, const dmat& mean, const dcube& loadings) {

    aw::assert_is_finite(data, innov_cov, mean, loadings);
    aw::assert_same(data.n_rows, innov_cov.n_slices, mean.n_cols, loadings.n_slices);


    npulong time_dim = data.n_rows; 

    dmat rtn_data;
    dcube rtn_innov_cov;
    dmat rtn_mean;
    dcube rtn_loadings;        

    for(npulong t=0; t<time_dim; ++t) { 
        
        drow temp_data; 
        dvec temp_mean;
        dmat temp_innov_cov;
        dmat temp_loadings;
        
        std::tie(temp_data, temp_innov_cov, temp_mean, temp_loadings) = 
            transform_data(data.row(t), extract_matrix(innov_cov,t), extract_row(mean, t).t(), 
                           extract_matrix(loadings, t)); 
        if (t == 0) {
            
            rtn_data = dmat(temp_data);
            rtn_mean = dmat(temp_mean); 
            rtn_loadings = dcube(temp_loadings.memptr(), temp_loadings.n_rows, temp_loadings.n_cols, 1);
            rtn_innov_cov = dcube(temp_innov_cov.memptr(), temp_innov_cov.n_rows, temp_innov_cov.n_cols, 1); 

        } else {

            aw::append_inplace(rtn_data, temp_data);
            aw::append_inplace(rtn_innov_cov, temp_innov_cov);
            aw::append_inplace(rtn_mean, temp_mean);             
            aw::append_inplace(rtn_loadings, temp_loadings); 
        } 

        
    }     

    return std::make_tuple(rtn_data, rtn_innov_cov, rtn_mean, rtn_loadings);

}





/* 
 * Checks if the dimensions are consistent 
 */
void check_valid_kalman_model(const dmat& data, const dmat& data_innov_var, const dmat& state_trans, 
                              const dmat& state_innov_var, const dvec& data_mean, 
                              const dmat& data_loadings, const dvec& state_mean) {
    if (! aw::check_square(data_innov_var, state_trans, state_innov_var)) {

        throw std::invalid_argument("Either one of the covariances is not square" 
                                 " or the transition matrix is not square.");
    }

    bool consistent_state_dim = aw::check_same(data_loadings.n_cols, state_trans.n_rows, state_innov_var.n_rows,
                                               state_mean.n_rows);
    if (! consistent_state_dim) {

        throw std::invalid_argument("The state dimensions are not consistent.");
    }

    bool consistent_data_dim = aw::check_same(data.n_cols, data_loadings.n_rows, data_innov_var.n_rows, 
                                              data_mean.n_rows);

    if (! consistent_data_dim) { 

       throw std::invalid_argument("The data dimensions are not consistent."); 

    }
}

/* 
 * Checks if a time-varying kalman filtering model has consistent dimensions. 
 */
void check_valid_kalman_model(const dmat& data, const dcube& data_innov_var, const dcube& state_trans, 
                              const dcube& state_innov_var, const dmat& data_mean, 
                              const dcube& data_loadings, const dmat& state_mean) {

   check_valid_kalman_model(data, data_innov_var.slice(0), state_trans.slice(0), state_innov_var.slice(0), 
                            data_mean.row(0).t(), data_loadings.slice(0), state_mean.row(0).t()); 

    if (! aw::check_same(data.n_rows, data_innov_var.n_slices, state_trans.n_slices, state_innov_var.n_slices,
                         data_mean.n_rows, data_loadings.n_slices, state_mean.n_rows)) {
        throw std::invalid_argument("The time-varying parameters do not have consistent dimensions");

    }
    
}

/**
 * This function uses Kalman filtering to compute the optimal linear estimates of the underlying states.
 *
 * It does not guarantee that the filtered covariances that are returned are positive semi-definite. It we do not
 * assume that the model is stationary, it assumes that the state before the first one observed equals zero. If we
 * do specify that it is stationary, it initially samples form the states' unconditional distribution.
 * It assumes that all of the matrices are in Fortran order. As is default for Armadillo. If you want to 
 * interface with Python, you must transpose the matrices before you feed them into this function.
 *
 * Note, I transpose the measurement equation covariance matrix before using it. If it is symmetric, this 
 * will not matter. If it is not, you  need to ensure that your matrix has the valid form. 
 * 
 * See the python wrapper function for complete documentation.
 */
template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
std::tuple<dmat, dcube, npdouble> kalman_filter(dmat data, T1 data_innov_var, const T2 &state_trans, 
                                                const T3& state_innov_var, T4 data_mean, T5 data_loadings,
                                                const T6 &state_mean, bool stationary) {

   check_valid_kalman_model(data, data_innov_var, state_trans, state_innov_var, data_mean, data_loadings, 
                           state_mean); 

    if (data.n_cols > 100) {
        std::tie(data, data_innov_var, data_mean, data_loadings) = 
            transform_data(data, data_innov_var, data_mean, data_loadings);
                                                                                   
    }

    auto time_dim = data.n_rows;
    auto data_dim = data.n_cols;
    auto state_dim = state_trans.n_rows;
        
    dmat filtered_means(time_dim, state_dim);
    dcube filtered_covs(state_dim, state_dim, time_dim);

    npdouble log_like = 0;
    dmat predictive_data_cov(data_dim, data_dim);
    dmat data_var(data_dim, data_dim);
    dmat data_precision(data_dim, data_dim);
       
    dmat trans = extract_matrix(state_trans,0).t();
    dmat state_var = extract_matrix(state_innov_var, 0).t();
    dvec dmean = extract_row(state_mean, 0).t();

    dmat predictive_state_cov = compute_initial_cov(trans, state_var, stationary); 
    dvec predictive_mean = compute_initial_mean(trans, dmean, stationary);  

    for (npulong day = 0; day < time_dim; ++day) {
         
        dmean = extract_row(data_mean, day).t();
        dmat loadings = extract_matrix(data_loadings, day).t();

        data_var = extract_matrix(data_innov_var, day).t();
        data_precision = cho_solve(lt_root_mat(data_var), arma::eye(data_dim, data_dim)); 
        
        if (day > 0) {
            trans = extract_matrix(state_trans, day-1).t();
            state_var = extract_matrix(state_innov_var, day-1).t();
            
            /* Predict the state */
            predictive_mean = extract_row(state_mean, day-1).t() + trans.t() * filtered_means.row(day-1).t();
            predictive_state_cov = trans.t() * filtered_covs.slice(day-1) * trans + state_var;

        }
        /* Predict the data */
        dvec data_prediction = dmean + loadings.t() * predictive_mean;
        dvec deviations = data.row(day).t() - data_prediction;
        predictive_data_cov = loadings.t() * predictive_state_cov * loadings + data_var;

        dmat cho_factored_weight = lt_root_mat(predictive_data_cov);
        
        /* Compute the increment of the log-likelihood */
        log_like -= arma::as_scalar(.5 * (data_dim * log2pi +  
                                    2 * arma::accu(arma::log(arma::abs(cho_factored_weight.diag()))) +
                                    deviations.t() * cho_solve(cho_factored_weight, deviations)));

        // Uupdate the state */ 
        filtered_covs.slice(day) = predictive_state_cov - (predictive_state_cov * loadings *
                                   cho_solve(cho_factored_weight, loadings.t() * predictive_state_cov));

        filtered_means.row(day) = arma::conv_to<dvec>::from(predictive_mean + predictive_state_cov * loadings *
                                                            cho_solve(cho_factored_weight, deviations)).t();

    }
    std::tuple<dmat, dcube, npdouble> returntup = std::make_tuple(filtered_means, filtered_covs, log_like);
    return returntup; 

        
}

void check_valid_kalman_smoother_model(const dmat& state_trans, const dmat& state_innov_var,
                                       const dvec& state_mean, const dmat& filtered_means, 
                                       const dcube& filtered_vars) {


    if (! aw::check_square(state_trans, state_innov_var)) {

        throw std::invalid_argument("Either the transition matrix or the innovation matrix are not square.");

    }

    bool consistent_state_dim =  aw::check_same(state_trans.n_cols, state_innov_var.n_cols, 
                                                filtered_means.n_cols, filtered_vars.n_cols, state_mean.n_rows);
    if (! consistent_state_dim) {

        throw std::invalid_argument("The state dimensions of the various parameters are not consistent.");
    } 

    if ( ! aw::check_same(filtered_means.n_rows, filtered_vars.n_slices)) {
        
        throw std::invalid_argument("The means and the variances have a different number of periods."); 
        
    } 

}


void check_valid_kalman_smoother_model(const dcube& state_trans, const dcube& state_innov_var,
                                       const dmat& state_mean, const dmat& filtered_means, 
                                       const dcube& filtered_vars) {

    
    check_valid_kalman_smoother_model(state_trans,state_innov_var,state_mean, filtered_means,filtered_vars);

    bool consistent_time_dim = aw::check_same(state_trans.n_slices, state_innov_var.n_slices, state_mean.n_cols,
                                              filtered_means.n_rows, filtered_vars.n_slices);


    if (! consistent_time_dim) {
        
        throw std::invalid_argument("The time-varying parameters do not have the same number of periods " 
                                    "as the filtered values."); 

    }    

}

/*
 * A Kalman smoother.  It uses the same notation as means and parameters as the one above. See the python wrapper
 * function for a complete explanation of the parameters.
 *
 * Returns
 * -------
 * draw : (T, K) ndarray of floats
 *       A draw from the posterior of the states.
 * smoothed_means : (T, K) ndarray of floats
 *    The mean of the posteiror state.
 * filtered_variances : (T,K,K) ndarray of floats
 *        The covariances of the states.
 */
template<typename T1, typename T2, typename T3>
std::tuple<dmat, dmat, dcube> kalman_smoother(T1 const &state_trans, T2 const &state_innov_var,
                                              T3 const &state_mean, const dmat &filtered_means,
                                              const dcube &filtered_vars) {

    check_valid_kalman_smoother_model(state_trans,state_innov_var,state_mean, filtered_means,filtered_vars);

    arma::arma_rng::set_seed_random();
    npulong time_dim = filtered_means.n_rows;
    npulong state_dim = filtered_means.n_cols;
    dmat draws(arma::size(filtered_means));
    dmat smoothed_state_means(arma::size(filtered_means));
    dcube smoothed_state_covs(arma::size(filtered_vars));
    dmat left_root = lt_root_mat(filtered_vars.slice(time_dim-1));
    draws.row(time_dim-1) = filtered_means.row(time_dim-1) + randn(1, state_dim) * left_root.t();
    smoothed_state_means.row(time_dim-1) = filtered_means.row(time_dim - 1);
    smoothed_state_covs.slice(time_dim-1) = filtered_vars.slice(time_dim-1);
        
    for (arma::sword day = time_dim - 2; day >= 0; --day) {
    
        dmat state_var = extract_matrix(state_innov_var, day).t();
        dmat trans = extract_matrix(state_trans, day).t();
        dvec mean = extract_row(state_mean, day).t();
        dmat filtered_cov_est = filtered_vars.slice(day);
        dvec filtered_mean_est = filtered_means.row(day).t();

        dmat cov_trans_product = filtered_cov_est * trans;
        dvec deviations = draws.row(day + 1).t() - (mean + trans.t() * filtered_mean_est);
        dmat smoothed_predictive_cov = make_symmetric(trans.t() * cov_trans_product + state_var);
        dmat root_predictive_cov = lt_root_mat(smoothed_predictive_cov);

        smoothed_state_means.row(day) = filtered_mean_est.t() + (cov_trans_product * cho_solve(
                                        root_predictive_cov, deviations)).t();
        smoothed_state_covs.slice(day) = make_symmetric(filtered_cov_est - cov_trans_product * cho_solve(
                                         root_predictive_cov, cov_trans_product.t()));

        dmat root_smoothed_cov = lt_root_mat(smoothed_state_covs.slice(day));
        draws.row(day) = smoothed_state_means.row(day) + randn(1, state_dim) *  root_smoothed_cov.t(); 
    }

    return std::make_tuple(draws, smoothed_state_means, smoothed_state_covs);
}


/*
 * Let A be a nrows by ncols matrix, and vec_outer_prod = vec(A) vec(A') then this calculates vec(A') vec(A)
 *
 *  Essentially, what this is doing is calculating the implied indices of the A in the reverse major order, and then
 *  uses that to calculate the implied index mapping between vec(A) vec(A') and vec(A') vec(A).  It is useful because
 *  it can be used to calculate the variance of the vectorization of A from the variance of the vectorization of A'.
 */
inline dmat switch_storage_order_variance_matrix(const dmat& vec_outer_prod, npulong n_cols, npulong n_rows) {



    assert(vec_outer_prod.n_cols == n_cols * n_rows);
    assert(vec_outer_prod.n_rows == n_cols * n_rows);
    
    dmat reordered_mat(n_cols, n_cols, arma::fill::zeros);
    /* Given an index of the vec(A) where A.shape = (ncols, nrows) computes the index of vec(A') of the same element. */
    auto extract_order_switched_idx = [](const npulong  m, const npulong n_cols) {
                                             auto i = m / n_cols;
                                             auto j = m % n_cols;
                                             return j * n_cols + i; 
                                        };
         
    for(npulong n=0; n<n_cols * n_rows; ++n) {

        auto n_prime = extract_order_switched_idx(n, n_cols);
        for(npulong m=0; m<n_cols * n_rows; ++m) {
            auto m_prime = extract_order_switched_idx(m, n_cols);
            
            reordered_mat(m_prime, n_prime) = vec_outer_prod(m, n);
            
        }
    }
    return reordered_mat;
}

inline
std::tuple<dmat, dmat> matrix_coeff_data_moments(const dmat& regressor, const dmat& regressand,
                                                 const dmat& innovation_var) { 
    
    
    const dmat xprimey = regressor.t() * regressand; 
    const dmat xprimex = regressor.t() * regressor;
    
    const dmat root_innovation_var = lt_root_mat(innovation_var.t()); 
    const dmat inv_innovation_var = cho_solve(root_innovation_var, arma::eye(arma::size(innovation_var))); 
    const dmat trans_precision = kron(inv_innovation_var, xprimex);
    const dmat mean = arma::vectorise(arma::solve(xprimex, xprimey)); 
    
    return std::make_tuple(trans_precision, mean);

}

inline
std::tuple<dmat, dmat> add_prior(const dmat& data_precision, const dmat& data_mean, const dmat& prior_mean, 
                                 const dmat& prior_var, npulong state_dim) { 
    
    const dmat prior_precision = kron(pinv(prior_var), arma::eye(state_dim, state_dim));     
    const dmat trans_precision = data_precision + prior_precision; 

    dmat broadcasted_prior_mean; 

    if (prior_mean.size() == trans_precision.n_cols) { 
        broadcasted_prior_mean = prior_mean; 
    } else { 
        assert(prior_mean.n_elem == trans_precision.n_cols / state_dim);  
        broadcasted_prior_mean = arma::vectorise(arma::repmat(arma::vectorise(prior_mean).t(), state_dim, 1)); 

    }
    const dmat trans_mean = data_precision * data_mean + prior_precision * broadcasted_prior_mean;  
   
    return std::make_tuple(trans_precision, trans_mean);  

}

inline std::tuple<dmat, dmat> matrix_coeff_moments(const dmat& regressor, const dmat& regressand,
                                                   const dmat& innovation_var, const dmat& prior_mean,
                                                   const dmat& prior_var) {

    const auto data_dim = regressand.n_cols; 
    const auto state_dim = regressor.n_cols; 

    aw::assert_same_n_rows(regressor, regressand);
    aw::assert_same_n_cols(regressand, innovation_var, prior_mean, prior_var);
    assert(prior_var.is_square());
    assert(innovation_var.is_square()); 

    dmat data_precision, data_mean;
    dmat trans_precision, trans_mean; 
    
    std::tie(data_precision, data_mean) = matrix_coeff_data_moments(regressor, regressand, innovation_var);
    std::tie(trans_precision, trans_mean) = add_prior(data_precision, data_mean, prior_mean, prior_var,
                                                      state_dim); 
    
    const dmat trans_precision_factored = lt_root_mat(trans_precision); 
    dmat mean = arma::reshape(cho_solve(trans_precision_factored, trans_mean), state_dim, data_dim);
    
    return std::make_tuple(dmat(trans_precision_factored.t()), mean);
}


inline 
std::tuple<dmat, dmat> matrix_coeff_moments(const dmat& regressor, const dmat& regressand, 
                                            const dmat& innovation_var) {
    
        
    auto data_dim = regressand.n_cols;
    auto state_dim = regressor.n_cols; 
    dmat trans_precision, vec_mean;
 
    std::tie(trans_precision, vec_mean) = matrix_coeff_data_moments(regressor, regressand, innovation_var); 
            
    const dmat trans_precision_factored = lt_root_mat(trans_precision).t(); 
    const dmat mean = arma::reshape(vec_mean, state_dim, data_dim);  

    return std::make_tuple(trans_precision_factored, mean);  
    

}


inline
std::tuple<dmat, dmat> matrix_coeff_moments_hetero_helper(const dmat& regressor, const dmat& regressand, 
                                                          const dcube& innovation_var) {



    const auto time_dim = regressor.n_rows;
    const auto mean_size = regressor.n_cols * regressand.n_cols; 
        
    dvec unweighted_mean(mean_size, arma::fill::zeros);
    dmat precision(mean_size, mean_size, arma::fill::zeros);

    
    const dmat transposed_regressor = regressor.t();
    const dmat transposed_regressand = regressand.t(); 
    #pragma omp parallel shared(unweighted_mean, precision) 
    {
        dvec thread_unweighted_mean(mean_size, arma::fill::zeros); 
        dmat thread_precision(mean_size, mean_size, arma::fill::zeros); 

        #pragma omp for
        for(npulong i=0; i<time_dim; ++i) {

            
            dmat day_precision;
            dmat day_mean;
             
            std::tie(day_precision, day_mean) = kal::matrix_coeff_data_moments(transposed_regressor.col(i).t(),
                                                                               transposed_regressand.col(i).t(),
                                                                               innovation_var.slice(i)); 
            thread_unweighted_mean += day_precision * day_mean; 
            thread_precision += day_precision;  
        }
    
        #pragma omp critical 
        {
            unweighted_mean += thread_unweighted_mean;
            precision += thread_precision;
        }
    }
    return std::make_tuple(precision, unweighted_mean);

}


inline
std::tuple<dmat, dmat> matrix_coeff_moments(const dmat& regressor, const dmat& regressand, 
                                            const dcube& innovation_var) { 
   
    const auto state_dim = regressor.n_cols;
    const auto data_dim = regressand.n_cols;
  
    dvec unweighted_mean(state_dim * data_dim, arma::fill::zeros);
    dmat precision(state_dim * data_dim, state_dim * data_dim, arma::fill::zeros);
    std::tie(precision, unweighted_mean) = matrix_coeff_moments_hetero_helper(regressor, regressand,
                                                                              innovation_var);                                                        
    dmat trans_precision_factored = lt_root_mat(precision);
    dmat mean = arma::reshape(cho_solve(trans_precision_factored, unweighted_mean).t(), state_dim, data_dim);

    return std::make_tuple(dmat(trans_precision_factored.t()), mean);
}
                                          


inline
std::tuple<dmat, dmat> matrix_coeff_moments(const dmat& regressor, const dmat& regressand, 
                                            const dcube& innovation_var, const dmat& prior_mean, 
                                            const dmat& prior_var) { 
   
    const auto state_dim = regressor.n_cols;
    const auto data_dim = regressand.n_cols;
  
    dmat data_precision, data_mean; 
    std::tie(data_precision, data_mean) = matrix_coeff_moments_hetero_helper(regressor, regressand, innovation_var);
    
    const dmat prior_precision = arma::kron(pinv(prior_var), arma::eye(state_dim, state_dim)); 
    const dmat broadcasted_prior_mean = arma::vectorise(arma::repmat(prior_mean.t(),state_dim, 1)); 
    data_precision += prior_precision;
    
    data_mean += prior_precision * broadcasted_prior_mean; 
    dmat trans_precision_factored = lt_root_mat(data_precision);
    dmat mean = arma::reshape(cho_solve(trans_precision_factored, data_mean).t(), state_dim, data_dim);

    return std::make_tuple(dmat(trans_precision_factored.t()), mean);
}


template<typename T1, typename T2>
npdouble multivariate_normal_log_density(const arma::Col<T1> &x, const T2 &mean, const arma::Mat<T1> &covariance) {

    npulong num_obs = x.size();
    npdouble log_det, sign;
    arma::log_det(log_det, sign, covariance);

    npdouble log_density = -.5 * (num_obs * log2pi + log_det + arma::dot(x - mean,
                                                                           arma::solve(covariance, (x - mean))));
    return log_density;

}


} 


#endif /* KALMAN_FILTER_H */ 
