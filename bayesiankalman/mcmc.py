import itertools
import tables
import numpy as np
import pandas as pd
from numpy import linalg as nplin
from numpy.linalg import pinv
from scipy import stats, special, linalg as scilin
from sklearn.preprocessing import normalize
from collections.abc import Iterable
from itertools import islice, repeat
from functools import partial
from statsmodels.nonparametric import kde
from bayesiankalman import csimulate_var, cmultivariate_normal_log_density, cscalar_normal_log_density
from bayesiankalman import cmatrix_coeff_moments, cmatrix_coeff_moments_no_prior
from bayesiankalman import cmatrix_coeff_moments_hetero, cmatrix_coeff_moments_hetero_no_prior


def simulate_stationary_mat(state_dim):
    """
     This function simply produces a random symmetric stationary VAR matrix.

    It does this by taking the eigendecomposition of a symmetric matrix and replacing it with values between 0 and 1.
    This will give the output as a symmetric matrix.

    Parameters
    ----------
    state_dim : positive int
        The number of states.

    Returns
    -------
        stationary_mat : 2d numpy array

    """
    starting_point = np.random.normal(0, 1, (state_dim, state_dim))
    left_mat, singular_values, right_mat = scilin.svd(starting_point)
    new_singular_values = np.random.uniform(0, .99, state_dim)
    stationary_mat = np.dot(left_mat, np.dot((new_singular_values *
                                              np.eye(state_dim)), right_mat))

    return stationary_mat


def draw_matrix_coefficient(regressor, regressand, prior_mean, prior_var, innovation_var):
    """
    This function computes a draw from the conditional poster of the coefficient from a ols-type regression.

    In other words, we have (Y = XB + E), where E dist Normal(0, innovation_variance). It provides the poster
    estimate of B in this context.  It also assumes that the prior for B is normal, so that we can use conjugate
    information.
    
    Parameters
    ----------
    regressor : (L, H) ndarray of floats
        The regressor from the ols-type equation.
    regressand : (L, J) ndarray of floats
        The regressand from the ols-type equation.
    prior_mean : (J, H) ndarray of floats
    The prior mean for the coefficient.
    prior_var : (J * H, J * H) ndarray of floats
        The prior variance for the vector form for the coefficient.
    innovation_var : (H,H) ndarray of floats
        The variance of the errors from the initial equation.

    Returns
    -------
    draw : (J,H) ndarray of floats
        The draw from the conditional posterior mean.
    """

    trans_precision_factored, mean = matrix_coeff_moments(prior_mean=prior_mean,
                                                          prior_var=prior_var,
                                                          regressand=regressand,
                                                          regressor=regressor,
                                                          innovation_var=innovation_var)

    draw = mean + scilin.solve_triangular(np.atleast_2d(trans_precision_factored),
                                          np.random.standard_normal(np.asarray(prior_mean).size),
                                          overwrite_b=True,
                                          check_finite=False,
                                          lower=False).reshape(mean.T.shape).T

    return np.squeeze(draw)


def matrix_coeff_moments(prior_mean, prior_var, regressand, regressor, innovation_var):
    """

    Parameters
    ----------
    prior_mean : ndarray
    prior_var : ndarray
    regressand : ndarray
    regressor : ndarray
    innovation_var : ndarray

    Returns
    -------
    mean : ndarray
    trans_precision_factored : ndarray
    """

    regressor, regressand = np.asarray(regressor), np.asarray(regressand)
    if regressor.ndim == 1:
        regressor = regressor.reshape((regressor.size, 1))
    if regressand.ndim == 1:
        regressand = regressand.reshape((regressand.size, 1))
    innovation_var = np.atleast_2d(innovation_var)

    heteroskedastic = innovation_var.shape[0] == regressor.shape[0] and innovation_var.shape[1] == regressand.shape[1]

    if (prior_mean is None) or (prior_var is None) or (np.all(np.logical_not(prior_var))):

        moment_func = cmatrix_coeff_moments_hetero_no_prior if heteroskedastic else cmatrix_coeff_moments_no_prior

        trans_precision_factored, mean = moment_func(regressor=regressor,
                                                     regressand=regressand,
                                                     innovation_var=innovation_var)
    else:
        moment_func = cmatrix_coeff_moments_hetero if heteroskedastic else cmatrix_coeff_moments

        trans_precision_factored, mean = moment_func(regressor=regressor,
                                                     regressand=regressand,
                                                     innovation_var=innovation_var,
                                                     prior_mean=prior_mean,
                                                     prior_var=prior_var)

    return trans_precision_factored, np.squeeze(mean)


def check_psd(matrix):
    """
    This function checks if the matrix is positive definite

    Parameters
    ----------
    matrix : (L,L) ndarray
        The matrix to check for positive definiteness.


    Returns
    -------
    is_psd : bool
        A boolean telling if the matrix is positive definite.
    """

    matrix = np.atleast_2d(matrix)

    try:
        nplin.cholesky(matrix)
        is_psd = True
    except nplin.linalg.LinAlgError:
        is_psd = False

    return is_psd


def make_stationary(matrix):
    """
    This function computes the nearest stationary matrix by forcing the eigenvalues to be less than 1 in modulus.

    It takes the eigendecomposition, and then divides the eigenvalues by 1.01 * modulus if they are greater than 1
    in modulus.

    Parameters
    ----------
    matrix : (L,L) ndarray of floats
        The matrix to make stationary.

    Returns
    -------
    stationary_mat : (L,L) ndarray of floats
        The closest stationary matrix to the given argument.

    """

    eigvals, eigvecs = scilin.eig(np.atleast_2d(matrix))
    eigvals[(np.abs(eigvals) >= 1)] /= 1.01 * np.abs(eigvals[(np.abs(eigvals) >= 1)])
    stationary_mat = np.real(eigvecs.dot((eigvals * pinv(eigvecs))))

    return stationary_mat


def make_symmetric(matrix):
    """
    This function simply averages the matrix with its transpose to get a symmetric matrix.

    :param matrix:
    :return:
    """
    matrix = np.atleast_2d(matrix)
    return 1 / 2 * (matrix + matrix.T)


def nearest_psd_matrix(matrix, tol=1e-3):
    """
    This function takes a matrix and returns the nearest positive definite matrix.

    Parameters
    ----------
    matrix : 2d ndarray
    tol : positive double

    """
    matrix = np.atleast_2d(make_symmetric(matrix))
    # If we're getting a negative scalar, we simply replace the value with a very small positive number.
    eigvals, eigvecs = scilin.eigh(matrix)
    eigvals = np.maximum(eigvals, tol)

    return (eigvecs * eigvals).dot(eigvecs.T)


def svd_psd_root(matrix):
    """
    This function uses the SVD decomposition to compute the left square root of a matrix.

    By using the SVD decomposition it is able to take square roots of matrices that are not full rank. The validity of
    the algorithm depends upon the matrix being positive semi-definite. Essentially, it finds the square root of the
    nearest positive semi-definite matrix, where nearness is defined in terms of the singular values.

    Parameters
    ----------
    matrix : 2d ndarray of floats
        The matrix to compute the square root of.

    Returns
    -------
    The left square root of the matrix.
    """

    # noinspection PyTypeChecker
    left_mat, singular_values, right_mat = nplin.svd(matrix, full_matrices=True)
    root_state_cov = np.atleast_2d(1 / 2 * (left_mat + right_mat.T) * np.sqrt(singular_values))

    return root_state_cov


def predict_state(last_state, state_trans, innovations, drift=None):
    """
    This generator yields draws from the predictive state.

    Parameters
    ----------
    drift
    last_state : (K) ndarray of floats
        The last state that we know.
    state_trans : (K,K) ndarray of floats
        The state transition matrix.
    innovations : (steps_ahead, K) ndarray of floats
        The innovations to add in each iteration.
    drift : K ndarray of floats
        The drift/intercept of the process.

    Yields
    -------
    A draw from the predictive distribution of the state until we use all of the innovations.
    """
    current_state = last_state
    drift = np.ravel(drift) if drift is not None else np.zeros(last_state.size)
    for innovation in innovations:
        current_state = drift + state_trans.dot(current_state) + innovation
        yield current_state


def simulate_var(time_dim, innov_var, var_coeff, initial_state=None, mean=None):
    """
   Simulate the model forward.

   The transition matrix is multiplied on the left.  I.e. Y = B X + e.
   Parameters
   ----------
   time_dim : positive int
   innov_var : 2d positive-definite ndarray of floats
   var_coeff : 2d ndarray of floats
   initial_state : 1d ndarray of floats
   mean : 1d ndarray of floats

   Returns
   -------
   simulated_data : 2d ndarray of floats

   Notes
   -----
   A Python implementation of a similar algorithm.

   var_coeff = np.atleast_2d(var_coeff)
   cross_dimension = var_coeff.shape[0]
   if initial_state is None:
       initial_state = np.zeros(cross_dimension)
   mean = np.asarray(mean) if mean is not None else np.zeros(cross_dimension)

   innovations = stats.multivariate_normal.rvs(cov=innov_var, size=time_dim)
   simulated_data = np.asanyarray([arr.T for arr in predict_state(initial_state, var_coeff, innovations, drift=mean)])

   return simulated_data
   """
    data_dim = len(var_coeff)
    var_coeff = np.reshape(var_coeff, (data_dim, data_dim))
    innov_var = np.reshape(innov_var, (data_dim, data_dim)).astype(np.float64)
    initial_state = (np.reshape(initial_state, data_dim).astype(np.float64)
                     if initial_state is not None else np.zeros(data_dim, dtype=np.float64))
    mean = (np.reshape(mean, data_dim).astype(np.float64)
            if mean is not None else np.zeros(data_dim, dtype=np.float64))

    data = csimulate_var(np.asfortranarray(var_coeff), innov_var, time_dim, initial_state, mean)

    return data


def rmse(estimate, truth):
    """
    This function simply computes the l2 norm (or root mean square error) of the distance between the two
     values. The two values must have the same size.
    Parameters
    ----------
    estimate : ndarray
    truth : ndarray

    Returns
    -------
    float

    """
    return np.sqrt(np.mean((np.ravel(estimate) - np.ravel(truth)) ** 2))


def create_lag_matrix(data, lags=None, external_regressors=None, prepend_ones=True):
    """

    Parameters
    ----------
    data : iterable or iterable of iterable of iterable, int
        If lags is not specified, this is assumed to be an iterable of pairs of the form [matrix, lag], where
        lag is either the maximum lag requested or an iterable containing the lags that are requested. If lags is
        specified, this is assumed to the 1-dimensional matrix for which to take the lags of. The lag can be zero.
    lags : int or iterable, optional
        This either contains the maximum requested lag or an iterable containing the lag. If it is given, data
        must be convertible to a 1-dimensional matrix.
    prepend_ones : bool, optional
        Whether to prepend a sequence of ones.
    external_regressors : iterable, optional
        External regressors to append the created matrix.

    Returns
    -------
    data_mat : 2d ndarray
    """
    if lags is not None:
        matrices = [(np.ravel(data), lags)]
    else:
        matrices = data

    row_with_lags = np.array([[mat.shape[0], np.amax(lag)] for mat, lag in matrices])
    num_effective_obs = np.amin(row_with_lags[:, 0]) - np.amax(row_with_lags[:, 1])
    assert num_effective_obs >= 0, "You must ask for more lags than you have elements even if they are " \
                                   "from different arrays."

    if prepend_ones:
        data_mat = np.ones((num_effective_obs, 1))
    else:
        data_mat = np.array([]).reshape((num_effective_obs, 0))
    for arr, lag in matrices:
        mat = np.atleast_2d(arr.T).T
        nrows = mat.shape[0]
        lag_iter = lag if isinstance(lag, Iterable) else range(1, lag + 1)
        data_mat = np.hstack([data_mat, *[mat[nrows - (num_effective_obs + i):(nrows - i)] for i in lag_iter]])

    if external_regressors is not None:
        external_regressors = np.atleast_2d(external_regressors.T).T
        data_mat = np.hstack([data_mat, external_regressors[external_regressors.shape[0] - num_effective_obs:]])

    return data_mat


def draw_innovation_var(regressor, regressand, coeff, prior_scale, prior_shape):
    """
    Computes the univariate innovation variance and then returns a draw from the inverse-gamma distribution
    Parameters
    --------
    regressor: array-like
    regressand : array-like
    coeff : array-like
    prior_scale : ndarray
    prior_shape : ndarray

    Returns
    -------
    ndarray

    """
    regressor, regressand = np.asarray(regressor), np.asarray(regressand)
    if regressand.ndim > 1 and regressand.shape[1] > 1:

        deviations = regressand - regressor.dot(coeff)
        scale = deviations.T.dot(deviations) + prior_scale
        df = regressand.shape[0] + prior_shape
        draw = stats.invwishart.rvs(df=df, scale=scale)
    else:
        post_scale = .5 * np.sum(np.ravel(regressand - regressor.dot(coeff)) ** 2) + prior_scale
        post_shape = .5 * len(regressand) + prior_shape

        draw = stats.invgamma.rvs(scale=post_scale, a=post_shape)

    return draw


def draw_truncated_multivariate_gaussian(mean=0, icov=None, upper_bounds=None, lower_bounds=None, size=1):
    """
    This returns draws from the truncated normal distribution using the gibbs sampler of Damien and Walker.
    Parameters
    ----------
    mean : 1d array-like
           The mean of (non-truncated) norm:qal distribution.
    icov : positive definite 2d array-like
            The inverse covariance matrix of the (non-truncated) normal distribution. If none is given it defaults to
            the identity. If a scalar is given the components are assumed to be independent with variance as given.
    upper_bounds : float or array-like
                   Upper bounds on the draw. If a float is given it assumed to have the same bound for all of
                   the variables. If None is given, then there is no bound.
    lower_bounds : float or array-like
                   Lower bounds on the draw. If a float is given it assumed to be the same for all of
                   the variables. If None is given then there is no bound.
    size : positive int
        The number of draws to return.

    Yields
    -------
    A draw from the truncated normal distribution.
    """

    import warnings
    warnings.warn("I am not sure this actually works.")
    # noinspection PyTypeChecker
    mean = np.asarray(mean, dtype='float').ravel()
    ndim = mean.size

    if icov is None:
        icov = np.eye(ndim, dtype='float')
    elif np.asarray(icov).size == 1:
        icov = icov * np.eye(ndim, dtype='float')
    else:
        icov = np.reshape(icov, newshape=(ndim, ndim))
        icov.dtype = 'float'

    if upper_bounds is None and lower_bounds is None:
        return np.random.multivariate_normal(mean=mean, cov=np.linalg.inv(icov), size=size)

    upper_bounds = np.asarray(upper_bounds, dtype='float')
    lower_bounds = np.asarray(lower_bounds, dtype='float')

    try:
        upper_bounds = upper_bounds.reshape(ndim)
    except ValueError:
        if upper_bounds.size == 1:
            upper_bounds = np.full(shape=ndim, fill_value=upper_bounds, dtype='float')
        else:
            raise ValueError('Upper_bound must be a scalar or the same size as the mean.')
    try:
        lower_bounds = lower_bounds.reshape(ndim)
    except ValueError:
        if lower_bounds.size == 1:
            lower_bounds = np.full(shape=ndim, fill_value=lower_bounds, dtype='float')

    new_draw = np.minimum(mean, upper_bounds)
    new_draw[np.isnan(new_draw)] = mean[np.isnan(new_draw)]
    new_draw = np.maximum(new_draw, lower_bounds)
    new_draw[np.isnan(new_draw)] = mean[np.isnan(new_draw)]

    def quantile_y(y, x):
        divergence = (x - mean).T.dot(icov).dot(x - mean)
        return_val = - 2 * np.log(1 - y) + divergence
        return return_val

    y_draw = quantile_y(np.random.uniform(), new_draw)
    bounds = np.empty(2)
    draws = np.empty((size, ndim), dtype='float')

    for draw_idx, _ in enumerate(draws):
        uniform_draw = np.random.uniform(0, 1, size=ndim + 1)

        for idx, u in enumerate(uniform_draw[:-1]):
            a_coeff = icov[idx, idx]
            excludes = [val for val in range(ndim) if val != idx]
            deviations = np.asarray((new_draw - mean)[excludes])
            weighted_deviations = icov[idx, excludes].dot(deviations)
            b_coeff = 2 * (weighted_deviations - mean[idx] * icov[idx, idx])
            c_coeff = np.asarray(deviations.dot(icov[excludes, :][:, excludes])).dot(deviations)
            if c_coeff.size == 0:
                c_coeff = np.array([0], dtype='float')
            c_coeff += icov[idx, idx] * mean[idx] ** 2 - 2 * mean[idx] * weighted_deviations - y_draw
            roots = np.sort(np.real(np.roots([a_coeff, b_coeff, c_coeff])))

            bounds[0] = np.maximum(roots[0], lower_bounds[idx]) if np.isfinite(lower_bounds[idx]) else roots[0]
            bounds[1] = np.minimum(roots[1], upper_bounds[idx]) if np.isfinite(upper_bounds[idx]) else roots[1]

            new_draw[idx] = u * (bounds[1] - bounds[0]) + bounds[0]

        y_draw = quantile_y(uniform_draw[-1], new_draw)
        draws[draw_idx] = new_draw

    return draws


def time_varying_reshape(arr, dims):
    """
    Combines a broadcasting and type checking into one function. It increases the dimension of the array by one.

    Parameters
    ----------
    arr : 2d or 3d ndarray
    dims : tuple of positive ints.

    Returns
    -------
    3d ndarray
    """
    try:
        arr = np.asarray(arr).reshape(dims)
    except ValueError:
        arr = np.asfortranarray(np.broadcast_to(np.reshape(arr, dims[1:]), dims))
    return arr


def leaf_keys(hash_table):
    """
    Returns all of the keys to all of the leaves in a possibly nested dictionary.

    Parameters
    --------
    hash_table : dictionary

    Yields
    -------
    key : immutable
    """

    for key, value in hash_table.items():
        if hasattr(value, 'items'):
            yield from leaf_keys(value)
        else:
            yield key


# noinspection PyUnresolvedReferences
def fan_plot(ax, data, percentiles, cm=None, alpha=None, labels=None, **kwargs):
    """
    Creates a fan plot using matplotlib and pandas.

    Parameters
    ----------
    ax : Axes
    The axes to draw to

    data : dataframe
    The data to plot.

    percentiles : tuple of positive ints
    The edges of the confidence bands.

    alpha : float, optional
    The alpha 'intensity' parameters.
    labels : tuple of strings, optional
    The labels of the artists.

    kwargs :
        Values to pass to the plot function.

    """

    from matplotlib import cm as mpl_cm

    cm = cm if cm is not None else mpl_cm.get_cmap()

    data_quantiles = pd.DataFrame(np.nanpercentile(data, percentiles, axis=1).T,
                                  index=data.index)
    num_sections = len(percentiles) // 2 + 1

    if len(percentiles) % 2 != 0:
        if labels is None:
            ax.plot(data_quantiles.iloc[:, num_sections - 1], **kwargs, zorder=num_sections+1)
        else:
            ax.plot(data_quantiles.iloc[:, num_sections - 1], label=labels[0], **kwargs, zorder=num_sections+1)

    if labels is None:
        for idx in range(num_sections):
            ax.fill_between(data_quantiles.index, data_quantiles.iloc[:, idx], data_quantiles.iloc[:, -(idx + 1)],
                            color=cm(idx / (num_sections -1), alpha=alpha), zorder=idx)
    else:
        for label, idx in zip(reversed(labels[1:]), reversed(range(num_sections-1))):
            ax.fill_between(data_quantiles.index, data_quantiles.iloc[:, idx], data_quantiles.iloc[:, -(idx + 1)],
                            color=cm(idx / (num_sections -1), alpha=alpha), label=label, zorder=idx)


def probability_integral_transform(reference_data, evaluated_data, progress_bar=None):
    """
    Computes the probability integral transform of the data using the empirical cdf of the predicted data.

    The reference_data is assumed to be of the form (horizon, data) dimensions where evaluated_data is of
    length evaluated_data.

    Parameters
    ----------
    reference_data : 2d ndarray or DataFrame
    evaluated_data : 1d ndarray or DataFrame
    progress_bar : tqdm instance
        One of the tqdm iterator wrappers.

    Returns
    -------
    return_points : DataFrame
    """

    if progress_bar:
        iterator = zip(progress_bar(np.asanyarray(reference_data)), np.asanyarray(evaluated_data))
    else:
        iterator = zip(np.asanyarray(reference_data),np.asanyarray(evaluated_data))

    return_points = np.fromiter((empirical_cdf(data_day, prediction) for data_day, prediction in iterator),
                                dtype=np.float64, count=evaluated_data.shape[0])

    return_df = pd.DataFrame(return_points, columns=['PIT'])

    if hasattr(evaluated_data, "index"):
        return_df.index  = index=evaluated_data.index

    return return_df


def empirical_cdf(data, eval_points):                                                                              
    """                                                                                                            
    Computes the empirical cdf of the data.                                                                        
    Parameters                                                                                                     
    ----------                                                                                                     
    data : array-like                                                                                              
    eval_points : array-like                                                                                       
    Returns                                                                                                        
    -------                                                                                                        
    return_vals : ndarray                                                                                          
    """                                                                                                            
    data = np.sort(data).reshape(data.size)                                                                        
    eval_points = np.ravel(eval_points)                                                                            
    return_vals = np.asarray([np.mean(np.less_equal(data, point), axis=0) for point in eval_points])               
                                                                                                                   
    return return_vals  


def normal_log_density(x, mean=0, scale=1, covariance=None):
    """"
   Calculates the log density for a normal random variable

   If both scale and covariance are given, one defaults to using the covariance.  (It is also worth noting that the
   scale is the standard deviation while covariance is a variance. This is the same convention as in scipy.

   Parameters
   --------
   x : double or 1d array-like, optional
       The value to evaluate the function at.
   scale : positive double, optional
       The standard deviation.
   mean : double or 1d array-like, optional
       The mean.
   covariance : 2d array-like, optional
       The covariance.
   """

    if covariance is None:
        return cscalar_normal_log_density(x=x, mean=mean, scale=scale)
    elif np.isscalar(covariance):
        return cscalar_normal_log_density(x=x, mean=mean, scale=np.sqrt(covariance))
    else:
        return cmultivariate_normal_log_density(x=x, mean=mean, covariance=covariance)


def estimate_model(drawing_func, nsims, prior):
    """
    Returns a dictionary of draws from the model.

    Parameters
    ----------
    drawing_func : callable
        A function that returns a draw from the posterior.
    nsims : positive int
        The number of draws.
    prior : dict
        The prior parameters. It is passed to a the drawing func.

    Returns
    -------
    results : 2d dict 'parameter_name' then index.
    """
    results = {key: np.empty((nsims,) + val.shape) for key, val in drawing_func(prior).items()}

    for idx in range(nsims):
        draw = drawing_func(prior)
        for key, val in draw.items():
            results[key][idx] = val.reshape(results[key][idx].shape)

    return results


def print_quantiles(results, excluded_vals, percentiles, *, burnin=0):
    """
    Prints the percentile bands of the values in results.

    Parameters
    ----------
    results : 2d dict
        Dictionary of results of with 'parameter_name' then index.
    excluded_vals : iterable of strings
        Values not to print quantiles for. It is useful for very large objects that are better to plot.
    percentiles : array-like of doubles between 0 and 100
    burnin: positive int, optional
        The number of draws to discard at the beginning.
    """
    for key, draw in iter(sorted(results.items())):

        if key not in excluded_vals:
            try:
                draw_arr = np.asanyarray(draw)
            except ValueError:
                if hasattr(draw[0], 'values'):
                    draw_arr = np.asanyarray([val.values for val in draw])
                else:
                    raise

            if draw_arr.size == 0:
                print(key)
                print("This parameter is empty.")
            else:
                return_pcts = np.squeeze(np.percentile(draw_arr[burnin:], percentiles, axis=0)).T

                if return_pcts.ndim <= 2:
                    quantiles = pd.DataFrame(return_pcts)
                    if quantiles.shape[1] != len(percentiles):
                        quantiles = quantiles.T
                    quantiles.columns = [str(num) + 'th pct' for num in percentiles]
                    print(key)
                    print(quantiles)


def save_hdf(draw, file, group=None, driver='H5FD_SEC2'):
    """
    This function saves the the draw to file in hdf format.

    If a pytables library is passed then it assumed that the caller takes care of opening and closing.
    Otherwise, this function opens and closes the file.

    Parameters
    ----------
    draw : a dict of floats
           The values to be saved. The keys are the subdirectory names.
    file : string or pytables file
              The name of the library. If None is passed, then the function doesn't do anything. This can be useful, if
              you want to dynamically decide whether to save a file.
    group : pytables group, optional.
            The string that contains the node name. If it is not given, it is assumed to be root.
    driver : string, optional
        The method for saving the parameters. It defaults to keeping the entire table in-memory for faster reads and
        writes.
    """
    if isinstance(file, str):
        with tables.open_file(file, mode='a', driver=driver) as file_hdf:
            try:
                group = file_hdf.root if group is None else getattr(file_hdf.root, group)
            except AttributeError:
                file_hdf.create_group(file_hdf.root, group)
                group = getattr(file_hdf.root, group)
            make_group(draw, file_hdf, group)
    elif file is None:
        return None
    else:
        make_group(draw, file, group)


def read_nested_hdf(file, *, driver='H5FD_SEC2'):
    """
    Reads the hdf file (file) into a nested dictionary.
    Parameters
    ----------
    file : str
    driver : string, optional
        The method for reading the parameters. The default loads the entire table into memory for faster reads and
        writes.

    Returns
    -------
    results : dict

    """
    with tables.open_file(file, driver=driver) as opened_file:
        results = read_node(opened_file, getattr(opened_file, 'root'))

    return results


def read_node(file, group):
    """
    Recursively iterates through the nodes.

    Parameters
    ----------
    file : str
    group : pytables group

    Returns
    ------
    nested dict
        The values contained in the group.


    """
    results = {}
    for node in group:
        if hasattr(node, 'read'):
            # noinspection PyProtectedMember
            results.update({node._v_name: node.read()})
        else:
            # noinspection PyProtectedMember
            results.update({node._v_name: read_node(file, node)})

    return results


def make_group(draw, file, group):
    """
    Fills the group given by group/name.

    Parameters
    ----------
    draw : dict of array-like
        The values to save.
    file : pytables file.
        It is assumed to be opened.
    group : pytables group or basestring
        The group that contains name. If None is given it is assumed to root.
    """

    for key, item in draw.items():
        if hasattr(item, 'items'):

            if key not in group:
                file.create_group(group, key)
            make_group(item, file, group=getattr(group, key))

        else:
            if key not in group:
                file.create_earray(where=group, name=key, obj=np.atleast_1d(item), expectedrows=1000)
            else:
                getattr(group, key).append(np.atleast_1d(item))


# noinspection PyTypeChecker
def gmm_rvs(size=1, pvals=1.0, means=0, scales=1, distribution=None):
    """
    This function returns a draw from a Gaussian mixture model.

    Parameters
    ----------
    pvals : array-like of positive floats
        The mixing proportions.
    size : positive int
           The number of samples to return
    means : array-like of floats
            The imeans of the components.
    scales : array-like of positive floats
               The standard deviations of the parameters.
    distribution : A distribution to draw from
        This is the distribution we are mixing over. It must accept a loc, scale, and size keyword arguments.

    Returns
    -------
    sample : ndarray of floats
             A sample from the mixture distribution.
    """
    pvals = normalize(np.reshape(pvals, (1, -1)), norm='l1')
    means = np.reshape(means, -1)
    scales = np.reshape(scales, -1)

    assert means.size == 1 or means.size == pvals.size, 'The imeans must be unique or common.'
    assert scales.size == 1 or scales.size == pvals.size, 'The variances must be unique or common.'

    if distribution is None:
        distribution = np.random.normal

    bincounts = np.random.multinomial(size, pvals=pvals.ravel())
    comp_it = itertools.zip_longest(means, itertools.zip_longest(scales, bincounts, fillvalue=scales[0]),
                                    fillvalue=means[0])
    draws = np.array([val for mean, (std, size) in comp_it
                      for val in distribution(loc=mean, scale=std, size=size) if size > 0]).ravel()

    return draws


def vectorized_norm_pdf(data, means, variances):
    """
    This function calculates the log pdf of a normal random variable multiplied by 1/ sqrt(2 pi)

    Parameters
    --------
    data : ndarray of floats
           The data to evaluate.
    means : ndarray of floats
           The imeans of the components
    variances : ndarray of positive floats
                The variances of the components.
    """

    data = np.reshape(data, (-1, 1))
    means = np.reshape(means, -1)
    variances = np.reshape(variances, -1)

    return np.exp(-1 / 2 * ((data - means) ** 2 / variances)) / np.sqrt(variances * 2 * np.pi)


def expanding_window_it(iterable, start=0):
    for idx in range(start, iterable.size):
        yield iterable[:idx]


def factor_model_state_trans(coeff):
    """
    Takes the coefficients from the AR representation of a model and makes the state transition matrix.

    Parameters
    ----------
    coeff

    Returns
    -------

    """
    coeff = np.ravel(coeff)
    if coeff.size == 0:
        return np.asarray([0])
    else:
        num_states = coeff.size
        state_trans = np.eye(num_states, k=-1)
        state_trans[0, :] = coeff

        return state_trans


def ols(regressor, regressand):
    regressor = np.atleast_2d(regressor.T).T
    regressand = np.asarray(regressand)
    return nplin.lstsq(regressor, regressand, rcond=-1)[0]


def frac_diff(data, d, truncation_order=30):
    """
    Fractionally differences the data.switch_storage_order_variance_matrix
    Parameters
    ----------
    data : array-like
    d : float, (negative integers are not allowed).
        differencing order
    truncation_order : positive int
        The truncation order of the differencing operation.

    Returns
    -------
    differenced_data : ndarray
    """

    coefficients = np.asarray([(-1) ** k * special.binom(d, k) for k in range(truncation_order + 1)])

    lagged_data = np.hstack([np.atleast_2d(data[truncation_order:].T).T,
                             create_lag_matrix(data, lags=truncation_order, prepend_ones=False)])

    differenced_data = lagged_data.dot(np.squeeze(coefficients))

    return differenced_data


def grouper(iterable, n, fillvalue=None, drop_unequal=False):                                                      
    """                                                                                                            
    Collect data into chunks or blocks                                                                             
                                                                                                                   
    If fillvalue is None, the last chunk may have a different size.                                                
                                                                                                                   
    Example                                                                                                        
    -------                                                                                                        
    grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx                                                                     
                                                                                                                   
    Parameters                                                                                                     
    ---------                                                                                                      
    iterable  : iterable                                                                                           
    n : positive int:                                                                                              
    fillvalue : optional                                                                                           
    drop_unequal : bool                                                                                            
                                                                                                                   
    Returns                                                                                                        
    ------                                                                                                         
    iterable of iterables                                                                                          
    """                                                                                                            
    
    if drop_unequal is True and fillvalue is None:
        args = [iter(iterable)] * n
        return zip(*args)
    
    if fillvalue is None:
        it = iter(iterable)
        sentinel = ()
    else:
        it = chain(iter(iterable), repeat(fillvalue))
        sentinel = (fillvalue,) * n

    return iter(lambda: tuple(islice(it, n)), sentinel)                             



def empirical_log_like(data, realization):
    """ 
    Compute the empirical log likelihood of the data and evaluate it at the realization

    Parameters
    ----------
    data: dataframe
    realization : scalar

    Returns
    float 
    """ 

    density = kde.KDEUnivariate(data)
    density.fit()
     
    return np.log(density.evaluate(realization))



def forecast_likelihood_evaluation(forecast, realizations):
    """ 
    Compute the empirical log likelihood of the data and evaluate it at each of the realizations.

    Parameters
    ----------
    data: dataframe
    realizations :  iterable

    Returns
    float 
    """ 

    return np.mean([empirical_log_like(data, val) for (_,data), val in zip(forecast.iterrows(), realizations)])


def cont_ranked_prob_score_in(arg, power=2):                                                                            
    """ Computes the cont_raked_prob_score at one point. """     
    
    (_, data), val = arg                                                                                                          
    cdf = partial(empirical_cdf, data=data)
                                                                                                           
    min_val = np.minimum(val, data.min())                                                                      
    max_val = np.maximum(val, data.max())           
    
    cdf = partial(empirical_cdf, data=data)

    unif_cdf_vals = np.linspace(start=min_val, stop=max_val, num=data.size)   
    val_idx = np.searchsorted(unif_cdf_vals, val, side='right')      
    
    cdf_vals = cdf(eval_points=unif_cdf_vals)
    
    diff = (max_val - min_val) / data.size                      
    return_val = diff * (np.sum(np.abs(cdf_vals[:val_idx])**power) + np.sum(np.abs(1-cdf_vals[val_idx:])**power))
                                                                                                                                             
    return return_val                


def cont_ranked_prob_score(forecast_data, true_values, power=2):
    """                                                                                                             
    Compute the continuously ranked probability score.                                                              
                                                                                                                    
    Parameters                                                                                                      
    --------                                                                                                        
    forecast_data : dataframe                                                                                       
    true_values : iterable                                                                                          
                                                                                                                    
    Returns                                                                                                         
    -------                                                                                                         
    float                                                                                                           
    """           
    forecast_data = pd.DataFrame(forecast_data)
    true_values = np.ravel(true_values)
                                                                                                                                                                                                                                                                                                                                                                            
    iterator = zip(forecast_data.iterrows(), true_values)                                                          

    crps = np.mean([cont_ranked_prob_score(arg, power=power) for arg in iterator])
                                                                                                                   
    return crps
