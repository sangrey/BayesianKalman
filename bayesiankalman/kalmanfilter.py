""" This module implements a kalman smoothing estimation. """
import warnings
import numpy as np
# noinspection PyPackageRequirements
import pandas as pd
from numpy import linalg as nplin
from numpy.linalg import inv
from scipy import linalg as scilin, stats
from . import mcmc
from .mcmc import time_varying_reshape
from bayesiankalman import ckalman_filter, ckalman_filter_time_varying
from bayesiankalman import ckalman_smoother, ckalman_smoother_time_varying

__author__ = 'sangrey'


class Kalman:
    """
    Implements the Carter-Kohn (1993) multi-move Gibbs smoothing algorithm.

    It updates both the state and the parameters. One can choose to update the data loadings or not.
    The 'draw' methods only return draws from the conditional posterior, they do not update the state.

    Derivations of the algorithms used can be seen in BayesianKalman.tex. Throughout the documentation we use T to
    refer to the number of periods, K to refer to the number of states, and N to refer to the dimension of the data.
    We assume that (K <= N).


    Attributes
    ----------
    _data : (T, N) ndarray of floats
        This contains the observed data.
    _states : (T, K) ndarray of floats
        This contains the underlying states, which we estimate.
    _state_innov_var : (K, K) ndarray of floats
        This is the covariance matrix of the state transition equation.
    _data_innov_var : (N, N) ndarray of floats
        This is the covariance matrix of the measurement equation.
    _data_mean : (N) ndarray of floats
        This is the mean of the data as defined in the data measurement equation.
    _state_means : (T, N) ndarray of floats
        This is a matrix of the conditional posterior means of the state vectors across the days.
    _state_vars : (T, K, K) ndarray of floats
        This is the 3d array of the conditional posterior covariances of the state vectors across the days.
    _data_loadings : (N,K) ndarray of floats
        This holds the matrix loadings of the data on to the underlying states.
    """

    def __init__(self, data, state_innov_var=None, data_innov_var=None,
                 state_trans=None, states=None, data_mean=None, state_mean=None,
                 smoothed_means=None, smoothed_vars=None, data_loadings=None, **kwargs):
        """
        This function initializes the various parameters to the values given or otherwise to reasonable initial values

        It assumes that you want to update all of the parameters except for the data loadings matrix.

        Parameters
        ----------
        data : (T * N) ndarray of floats
            This contains the observed data.
        states : (T * K) ndarray of floats
            This contains the underlying states, which we estimate.
        state_innov_var : (K * K) ndarray of floats
            This is the covariance matrix of the state transition equation.
        data_innov_var : (N * N) ndarray of floats
            This is the covariance matrix of the measurement equation.
        data_mean : (N) ndarray of floats
            This is the mean of the data as defined in the data measurement equation.
        smoothed_means : (T, N) ndarray of floats
            This is a matrix of the conditional posterior means of the state vectors across the days.
        smoothed_vars : (T, K, K) ndarray of floats
            This is the 3d array of the conditional posterior covariances of the state vectors across the days.
        data_loadings : (N,K) ndarray of floats
            This holds the matrix loadings of the data on to the underlying states.
        kwargs : dictionary
            This holds  various other parameters for use in nonstandard initializations.
        """

        self._counter = 0
        self._data = np.atleast_2d(data)

        if states is not None:
            self._states = np.atleast_2d(states)
        elif state_innov_var is not None:
            state_innov_var = np.atleast_2d(state_innov_var)
            assert state_innov_var.shape[0] == state_innov_var.shape[1], 'The state innovation variance is not square'
            self._states = np.zeros((self.time_dim, state_innov_var.shape[0]))
        else:
            if 'state_dim' not in kwargs:
                raise ValueError('Either the states must be initialized or the number of states must be given.')
            self._states = np.zeros(self.time_dim, kwargs['state_dim'])

        self.state_mean = np.asarray(state_mean) if state_mean is not None else np.zeros(self.state_dim)

        self._state_trans = .9 * np.identity(self.state_dim) if state_trans is None else np.asarray(state_trans)
        self._data_mean = np.zeros(self.state_dim) if data_mean is None else np.atleast_1d(data_mean)
        self._smoothed_means = (np.zeros((self.time_dim, self.state_dim))
                                if smoothed_means is None else np.atleast_2d(smoothed_means))
        self._data_loadings = np.eye(self.state_dim) if data_loadings is None else np.atleast_2d(data_loadings)
        self._data_innov_var = (np.full((self.data_dim, self.data_dim), np.var(self.data) / 4)
                                if data_innov_var is None else np.atleast_2d(data_innov_var))
        self._state_innov_var = (np.full_like(self.state_trans, np.var(self.data) / 4)
                                 if state_innov_var is None else np.atleast_2d(state_innov_var))

        if smoothed_vars is None:
            self._smoothed_vars = (2 * np.asarray(self.state_innov_var).flatten()[0]) * np.asanyarray([np.eye(
                self.state_dim, self.state_dim) for _ in range(self.time_dim)])
        else:
            self._smoothed_vars = np.atleast_2d(smoothed_vars)

        self._attributes_to_update = ['state_trans', 'data_mean', 'state_innov_var', 'data_innov_var', 'states']

        # I ensure that the various parameters have compatible shapes.
        assert self.state_trans.shape == (self.state_dim, self.state_dim), ('The state transition matrix does not '
                                                                            'have the correct shape')
        assert self.data_mean.shape == (self.data_dim,), 'The data mean does not have the correct shape'
        assert self.state_innov_var.shape == (self.state_dim, self.state_dim), ('The state innovation variance does '
                                                                                'not have the correct shape')
        assert self.data_innov_var.shape == (self.data_dim, self.data_dim), ('The data innovation variance '
                                                                             'does not have the correct '
                                                                             'shape')
        assert self.data_loadings.shape == (self.data_dim, self.state_dim), ('The data loadings matrix does not '
                                                                             'have the correct shape')
        assert self.states.shape[0] == self.time_dim, ('The data and the states do not have the same number of '
                                                       'periods.')

    def __call__(self, data, state_prior, data_prior):
        """
        Runs the Gibbs sampling algorithm updating the state of the class.

        The data is given here because it allows you to run this within a larger hierarchical model, and so it changes
        between iterations of the sampler.

        Parameters
        ----------
        data : (T * K) ndarray
            The data which has been assumed to be observed.
        state_prior : dict of dicts
            This dictionary contains all of the parameters of the prior for the state equation.
        data_prior : dict of dicts
            This dictionary contains all of the parameters of the prior for the state equation.

        Returns
        -------
            A dictionary of copies of the draws from the posterior of each of the components.
        """

        self.data = data
        self.counter += 1

        for name in self.attributes_to_update:
            if name != 'states':
                update_function = self.__getattribute__('draw_' + name)
                string_comps = name.split('_', maxsplit=1)

                if 'data' in string_comps:
                    self.__setattr__(name, update_function(data_prior[string_comps[1]]))
                else:
                    self.__setattr__(name, update_function(state_prior[string_comps[1]]))
            else:
                pass
                # self.states, self.smoothed_means, self.smoothed_vars = self.draw_smoothed_states()

        parameters = {'data_mean': self.data_mean.copy(), 'data_innov_var': self.data_innov_var.copy(),
                      'states': self.states.copy(), 'state_trans': self.state_trans.copy(),
                      'state_innov_var': self.state_innov_var.copy(), 'smoothed_means': self.smoothed_means.copy(),
                      'smoothed_vars': self.smoothed_vars.copy()}

        return parameters

    def __str__(self, rounding_precision=2):
        """
        I simply provide a well formatted way of displaying the underlying states.

        I use the formatting tools of a pandas dataframe.

        Parameters
        -------
        rounding_precision : int, optional
            The number of digits to round the output to.

        Returns
        -------
        A formatted string representation of the underlying states.

        """
        states = pd.DataFrame(self.states).round(rounding_precision)

        return str(states)

    def draw_state_trans(self, prior):
        """
        This function returns a draw from the conditional posterior of state transition matrix.

        Parameters
        ----------
        prior: dict
            This holds the parameters of the prior of the state transition matrix.

        Returns
        -------
        (K,K) ndarray of floats
            The matrix-valued draw from the conditional posterior of the state transition matrix.
        """

        regressor = mcmc.create_lag_matrix(self.states, lags=1, prepend_ones=False)
        try:
            regressand = self.states[1:] - self.state_mean
        except AttributeError:
            regressand = self.states[1:]

        draw = mcmc.draw_matrix_coefficient(regressor, regressand, prior['mean'], prior['variance'],
                                            self.state_innov_var)

        return draw

    def draw_data_loadings(self, prior):
        """
        This function returns a draw from the conditional posterior of data loadings.

       If K = 1, I impose the normalization that the central element equals 1.  If K = N, I impose the normalization
       that all of the diagonal elements are equal to 1.

        Parameters
        ----------
        prior: dict
            This holds the parameters of the prior of the state transition matrix.

        Returns
        -------
        (N,K) ndarray of floats
            The matrix-valued draw from the conditional posterior of the data loadings.
        """

        regressor = self.states
        regressand = self.data

        draw = mcmc.draw_matrix_coefficient(regressor, regressand, prior['mean'], prior['variance'],
                                            self.state_innov_var)
        if draw.shape[1] == 1:
            draw /= draw[draw.size // 2]
        elif draw.shape[0] == draw.shape[1]:
            draw = draw.dot(np.diag(np.sign(np.diag(draw))))

        return draw

    def draw_data_innov_var(self, prior):
        """
        This function draws from the conditional posterior of the data measurement covariance.

        I am using the bayesiankalman's parameterization, which is the same as that used in Wikipedia, but not the same
         as that in Diebold 2015.concatenate

        Parameters
        ----------
        prior: dict
            This holds the parameters of the prior of the data innovation variance.

        Returns
        -------
        (N,N) ndarray of floats
            A draw of the covariance of the data from its conditional posterior.

        """

        prior_alpha, prior_beta = prior['alpha'], prior['beta']

        post_alpha = prior_alpha + self.time_dim
        data_deviations = self.data - (self.data_mean + self.states.dot(self.data_loadings.T))
        post_beta = prior_beta + data_deviations.T.dot(data_deviations)

        try:
            draw = stats.invwishart.rvs(df=post_alpha, scale=mcmc.make_symmetric(post_beta))
        except nplin.linalg.LinAlgError:
            draw = stats.invwishart.rvs(df=post_alpha, scale=mcmc.nearest_psd_matrix(post_beta))
            warnings.warn('I had an issue with the data innovation variance being positive definite.')

        return draw

    def draw_state_innov_var(self, prior):
        """
        This function draws from the conditional posterior of the state transition covariance.

        I am using the bayesiankalman's parameterization, which is the same as that used in Wikipedia, but not the same
         as that in Diebold 2015.

        Parameters
        ----------
        prior: dict
            This holds the parameters of the prior of the state innovation covariance.

        Returns
        -------
        (K,K) ndarray of floats
            A draw of the covariance of the states from its conditional posterior.

        """

        prior_alpha, prior_beta = prior['alpha'], prior['beta']
        post_alpha = prior_alpha + self.time_dim - 1
        state_deviations = self.states[1:] - (self.states[:-1].dot(self.state_trans.T) + self.state_mean)
        post_beta = prior_beta + np.dot(state_deviations.T, state_deviations)
        try:
            draw = stats.invwishart.rvs(df=post_alpha, scale=mcmc.make_symmetric(post_beta))
        except nplin.linalg.LinAlgError:
            draw = stats.invwishart.rvs(df=post_alpha, scale=mcmc.nearest_psd_matrix(post_beta))
            warnings.warn('I had an issue with the state innovation variance being positive definite.')

        return draw

    def draw_data_mean(self, prior):
        """
        This function draws from the conditional posterior of data's mean  as defined in the data measurement equation

        Parameters
        ----------
        prior: dict of floats
            This holds the parameters of the prior of the mean of the data. It must have keys 'mean' and 'variance'
        Returns
        -------
        (N) ndarray of floats
            A draw of the mean of the data from its conditional posterior.

        """

        post_precision = mcmc.make_symmetric(self.time_dim * inv(self.data_innov_var) + inv(prior['variance']))
        try:
            factored_post_precision = scilin.cho_factor(post_precision)
        except nplin.linalg.LinAlgError:
            factored_post_precision = scilin.cho_factor(mcmc.nearest_psd_matrix(post_precision))
            warnings.warn('The data_mean posterior variance was not positive definite.')

        unnormalized_post_mean = (nplin.solve(prior['variance'], prior['mean']) + self.time_dim *
                                  nplin.solve(self.data_innov_var,
                                              np.mean(self.data - self.states.dot(self.data_loadings.T), axis=0)))
        unnormalized_draw = unnormalized_post_mean + np.random.standard_normal(self.data_dim)
        draw = scilin.cho_solve(factored_post_precision, unnormalized_draw)

        return draw

    def draw_filtered_states(self, stationary=False):
        """
        This function uses Kalman filtering to compute the optimal linear estimates of the underlying states.

        It does not guarantee that the filtered covariances that are returned are positive semi-definite.

        Parameters
        ----------
        stationary : Optional(bool)
            Whether the states are assumed to be stationary.

        Returns
        -------
        filtered_means : (T, K) ndarray of floats
            This contains optimal linear estimates of the underlying states using data up until the period in question.
        filtered_variances : (T,K,K) ndarray of floats
            This contains estimates of the uncertainty of the underlying states in the form of the covariance within
            each period.
        log_like : float
            The log-likelihood of the data under an assumption of normality.
        """

        filtered_means, filtered_vars, log_like = kalman_filter(data=self.data, data_innov_var=self.data_innov_var,
                                                                state_trans=self.state_trans,
                                                                state_innov_var=self.state_innov_var,
                                                                data_mean=self.data_mean,
                                                                data_loadings=self.data_loadings,
                                                                state_mean=self.state_mean,
                                                                stationary=stationary,
                                                                time_varying=False,
                                                                check_correct_size=True)

        return filtered_means, filtered_vars, log_like

    def draw_smoothed_states(self, stationary=False):
        """
        This function provides draws from the conditional posterior of the underlying states.

        It uses the Carter-Kohn (1993) algorithm to provide a draw from the conditional poster of the states assuming
        normality.  It assumes the kalman_filter algorithm has already been run.

        Parameters
        ----------
        stationary : bool
            Whether the states are assumed to be stationary

        Returns
        -------
        draws: (T, K) ndarray of floats
            The draw from the conditional posterior.
        filtered_means : (T, K) ndarray of floats
            This contains optimal linear estimates of the underlying states using data up until the period in question.
        filtered_variances : (T,K,K) ndarray of floats
            This contains estimates of the uncertainty of the underlying states in the form of the covariance within
            each period.
        """
        draws, smoothed_state_means, smoothed_state_vars = kalman_smoother(state_trans=self.state_trans,
                                                                           state_innov_var=self.state_innov_var,
                                                                           state_mean=self.state_mean,
                                                                           data=self.data,
                                                                           data_innov_var=self.data_innov_var,
                                                                           data_mean=self.data_mean,
                                                                           data_loadings=self.data_loadings,
                                                                           stationary=stationary,
                                                                           time_varying=False,
                                                                           check_correct_size=True)

        return draws, smoothed_state_means, smoothed_state_vars

    def forecast_model(self, steps_ahead):
        """
        This function uses the last state given and the parameter to draw an estimate from the predictive posterior.

        Parameters
        -------
        steps_ahead : int
            The number of steps ahead to forecast

        Returns
        -------
        future_states : (steps_ahead, K) ndarray of floats
            A draw of the future states from the predictive posterior.
        future_states : (steps_ahead, N) ndarray of floats
            A draw of the future data from the predictive posterior.
        """

        future_data, future_states = forecast_statespace(steps_ahead, self.state_innov_var, self.state_trans,
                                                         self.data_innov_var, self.data_loadings, self.states[-1],
                                                         state_mean=self.state_mean, data_mean=self.data_mean)

        return future_data, future_states

    def log_like(self):
        """
        Calculates the likelihood of tbe model using the kalman_so filter

        Returns
        -------
        log_like : float
            the log-likelihood of the model.
        """
        log_like = kalman_log_like(data=self.data, data_loadings=self.data_loadings, data_mean=self.data_mean,
                                   data_innov_var=self.data_innov_var, state_trans=self.state_trans,
                                   state_innov_var=self.state_innov_var)
        return log_like

    @property
    def state_dim(self):
        return self.states.shape[1]

    @property
    def time_dim(self):
        return self.data.shape[0]

    @property
    def data_dim(self):
        return self.data.shape[1]

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.time_dim, self.data_dim), 'The data to be set has incorrect dimensions'
        self._data = val

    @property
    def states(self):
        return self._states

    @property
    def attributes_to_update(self):
        return self._attributes_to_update

    @attributes_to_update.setter
    def attributes_to_update(self, val):
        """
        The function updates the attributes that you want to update

        Parameters
        ----------
        val : list of strings

        """
        try:
            for name in val:
                if name != 'states':
                    self.__getattribute__('draw_' + name)
        except AttributeError:
            raise (AttributeError, 'Either that parameter does not exist or no method exists to update it.')

        self._attributes_to_update = val

    @states.setter
    def states(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.time_dim, self.state_dim), 'The new states do not have the correct dimensions'
        self._states = np.ascontiguousarray(val, dtype=np.float64)

    @property
    def state_innov_var(self):
        return self._state_innov_var

    @state_innov_var.setter
    def state_innov_var(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.state_dim, self.state_dim), ('The new state innovation variance does not have '
                                                               'the correct variance.')
        self._state_innov_var = np.ascontiguousarray(val, np.float64)

    @property
    def state_trans(self):
        return self._state_trans

    @state_trans.setter
    def state_trans(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.state_dim, self.state_dim), ('The new state transition matrix does not have the'
                                                               'correct shape.')
        self._state_trans = np.ascontiguousarray(np.atleast_2d(val), np.float64)

    @property
    def data_innov_var(self):
        return self._data_innov_var

    @data_innov_var.setter
    def data_innov_var(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.data_dim, self.data_dim), ('The new data innovation variance does not '
                                                             'have the correct shape.')
        self._data_innov_var = np.ascontiguousarray(np.atleast_2d(val), np.float64)

    @property
    def data_mean(self):
        return self._data_mean

    @data_mean.setter
    def data_mean(self, val):
        val = np.atleast_1d(val)
        assert val.shape == (self.data_dim,), 'The new mean of the data does not have the correct shape'
        self._data_mean = np.ascontiguousarray(val, np.float64)

    @property
    def smoothed_means(self):
        return self._smoothed_means

    @smoothed_means.setter
    def smoothed_means(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.time_dim, self.state_dim), ('The new means of the states does not have the'
                                                              'correct shape')
        self._smoothed_means = np.ascontiguousarray(val, np.float64)

    @property
    def smoothed_vars(self):
        return self._smoothed_vars

    @smoothed_vars.setter
    def smoothed_vars(self, val):
        val = np.atleast_3d(val)
        assert val.shape == (self.time_dim, self.state_dim, self.state_dim), ('The new covariances for the states'
                                                                              'do not have the correct shape')
        self._smoothed_vars = np.ascontiguousarray(val, np.float64)

    @property
    def data_loadings(self):
        return self._data_loadings

    @data_loadings.setter
    def data_loadings(self, val):
        val = np.atleast_2d(val)
        assert val.shape == (self.data_dim, self.state_dim), ('The data loadings do not have the correct '
                                                              'dimension.')
        self._data_loadings = np.ascontiguousarray(val, np.float64)

    @property
    def counter(self):
        return self._counter

    @counter.setter
    def counter(self, val):
        self._counter = val


# noinspection PyUnresolvedReferences
def simulate_model(data_mean, state_trans, data_loadings, state_innov_var, data_innov_var, state_mean=None, states=None,
                   time_dim=100):
    """
    This takes the the given parameters and simulates from the model.

    Parameters
    ----------
    data_mean : (N) ndarray of floats
        The mean of the data
    state_trans : (K,K) ndarray of floats
        The transition matrix of the states.
    data_loadings : (N,K) ndarray of floats
        The loadings of the data onto the state.
    state_innov_var : (K,K) ndarray of floats
       The variance of the innovations in the state transition equation.
    data_innov_var : (N,N) ndarray of floats
        The variance of the innovations in the measurement equation.
    state_mean : (K,) ndarray of floats
    states : (T,K) ndarray of floats, optional
        The latent states.
    time_dim : int, optional
        The number of periods to simulate. It will be ignored if states is given.

    Returns
    -------
        A Kalman object with the parameters given and simulated data and states.
    """

    data_innov_var = np.atleast_2d(data_innov_var)
    state_innov_var = np.atleast_2d(state_innov_var)
    data_dim = data_innov_var.shape[0]
    state_dim = state_innov_var.shape[0]
    data_loadings = data_loadings.reshape((data_dim, state_dim))
    state_trans = np.reshape(state_trans, (state_dim, state_dim))
    state_mean = np.asarray(state_mean) if state_mean is not None else np.zeros(state_dim)

    if states is None and time_dim is not None:

        states = mcmc.simulate_var(time_dim, state_innov_var, state_trans, mean=state_mean)
    else:
        raise ValueError('Either states or time_dim must be filled.')

    time_dim = states.shape[0]
    data_innovations = stats.multivariate_normal.rvs(cov=data_innov_var, size=time_dim)
    data = np.atleast_2d(data_mean) + states.dot(data_loadings.T) + data_innovations.reshape(
        time_dim, data_dim)

    model = Kalman(
        data=data,
        data_innov_var=data_innov_var,
        state_innov_var=state_innov_var,
        state_trans=state_trans,
        data_mean=data_mean,
        states=states,
        data_loadings=data_loadings,
        state_mean=state_mean
    )

    return model


def kalman_log_like(**kwargs):
    """
    This function simply forwards all of its arguments to the kalman_filter function and returns the likelihood.

    Parameters
    -------
    **kwargs : dict
        These are simply passed to  the kalman_filter function.
    Returns
    -------
    log_like : float
        The log-likelihood of the model
    """

    _, _, log_like = kalman_filter(**kwargs)

    return log_like


def kalman_filter(data, data_innov_var, state_trans, state_innov_var, data_mean=None, data_loadings=None,
                  state_mean=None, stationary=False, check_correct_size=True, time_varying=False):
    """
   This function draws from the conditional posterior of the underlying states in a gaussian statespace model.

   It uses the Carter-Kohn (1993) algorithm to provide a draw from the conditional poster of the states assuming
   normality.  It assumes the kalman_filter algorithm has already been run. Unlike the smoothing algorithm, it does
   guarantee that the returned covariances are positive semi-definite. If either of smoothed_means or smoothed_vars
   is missing it runs the kalman filter to estimate them. As a result, if you do not specify them, you must specify
   the parameters necessary to run the Kalman filtering algorithm.

   It allows for time-varying parameters.  In this case the first dimension should be time. If time_varying is true
   it does expand the dimensionality of the parameters all of the parameters in this case. As a result, if you only
   need a few of the parameters to vary with time and the code is really slow, you may need to write methods that do
   not do this. The C++ code can handle any specification, but a wrapper would have to be added.

   Parameters
   -------
   data : Optional (2d ndarray of floats)
   data_innov_var : Optional (2d ndarray of floats)
       The innovation variance of the measurement equation.
   state_trans : 2d ndarray of floats
       The transition matrix for the state equation.
   state_innov_var : 2d numpy array of floats
       The innovation variance in the state equation.
   data_mean : Optional (1d ndarray of floats)
       The means of the data
   data_loadings : Optional (2d ndarray of floats)
       It is data_dim by state_dim
   state_mean : Optional (1d ndarray of floats)
       Assumed to be zero if it is not given.
   stationary : Optional (bool)
       Whether the model is assumed to be stationary.
   check_correct_size  : Optional (bool)
       Whether to ensure that the input is valid shape. If it false a pointer is passed to directly to the
       C code.
   time_varying : Optional (bool)
       Whether the parameters vary over time.

   Returns
   -------
   draws: 2d ndarray of floats
       The draw from the conditional posterior.
   filtered_means : 2d ndarray of floats
       This contains optimal linear estimates of the underlying states using data up until the period in question.
   filtered_variances : 3d ndarray of floats
       This contains estimates of the uncertainty of the underlying states in the form of the covariance within
       each period.
   """
    data = np.asarray(data)
    if data.ndim == 1:
        time_dim, data_dim = data.size, 1
    else:
        time_dim, data_dim = data.shape

    state_trans = np.atleast_2d(state_trans)
    if state_trans.shape[1] == time_dim and state_trans.shape[0] != state_trans.shape[1]:
        state_trans = state_trans.T

    state_dim = state_trans.shape[1]

    if check_correct_size or data_mean is None or data_loadings is None or state_mean is None:

        shape = (time_dim, state_dim, data_dim)
        # noinspection PyTupleAssignmentBalance
        data_innov_var, data_loadings, state_trans, state_innov_var, data_mean, state_mean \
            = ensure_valid_shapes(shape, data_innov_var, data_loadings, state_trans,
                                  state_innov_var, data_mean, state_mean, time_varying)
    
    filter_func = ckalman_filter_time_varying if time_varying else ckalman_filter

    filtered_means, filtered_vars, log_like = filter_func(data=data,
                                                          data_innov_var=data_innov_var,
                                                          state_trans=state_trans,
                                                          state_innov_var=state_innov_var,
                                                          data_mean=data_mean,
                                                          data_loadings=data_loadings,
                                                          state_mean=state_mean,
                                                          stationary=stationary
                                                          )

    return filtered_means, filtered_vars, log_like


def kalman_smoother(state_trans, state_innov_var, state_mean=None, filtered_means=None, filtered_vars=None, data=None,
                    data_innov_var=None, data_mean=None, data_loadings=None, stationary=False, check_correct_size=True,
                    time_varying=False):
    """
   This function draws from the conditional posterior of the underlying states in a gaussian statespace model.

   It uses the Carter-Kohn (1993) algorithm to provide a draw from the conditional poster of the states assuming
   normality.  It assumes the kalman_filter algorithm has already been run. Unlike the smoothing algorithm, it does
   guarantee that the returned covariances are positive semi-definite. If either of smoothed_means or smoothed_vars
   is missing it runs the kalman filter to estimate them. As a result, if you do not specify them, you must specify
   the parameters necessary to run the Kalman filtering algorithm.

   It allows for time-varying parameters.  In this case the first dimension should be time. If time_varying is true
   it does expand the dimensionality of the parameters all of the parameters in this case. As a result, if you only
   need a few of the parameters to vary with time and the code is really slow, you may need to write methods that do
   not do this. The C++ code can handle any specification, but a wrapper would have to be added.

   Parameters
   -------
   state_trans : 2d ndarray of floats
       The transition matrix for the state equation.
   state_innov_var : 2d numpy array of floats
       The innovation variance in the state equation.
   state_mean : Optional (1d ndarray of floats)
       Assumed to be zero if it is not given.
   filtered_vars : Optional (3d ndarray of floats)
   filtered_means : Optional (2d ndarray of floats)
   data : Optional (2d ndarray of floats)
   data_innov_var : Optional (2d ndarray of floats)
       The innovation variance of the measurement equation.
   data_mean : Optional (1d ndarray of floats)
       The means of the data
   data_loadings : Optional (2d ndarray of floats)
       It is data_dim by state_dim
   stationary : Optional (bool)
       Whether the model is assumed to be stationary.
   check_correct_size  : Optional (bool)
       Whether to ensure that the input is valid shape and type. If it is false, a pointer to the data is passed
       directly to the C code.
   time_varying : Optional (bool)
       Whether the parameters vary over time.

   Returns
   -------
   draws: 2d ndarray of floats
       The draw from the conditional posterior.
   filtered_means : 2d ndarray of floats
       This contains optimal linear estimates of the underlying states using data up until the period in question.
   filtered_variances : 3d ndarray of floats
       This contains estimates of the uncertainty of the underlying states in the form of the covariance within
       each period.
   """
    filtered = False

    if filtered_means is None or filtered_vars is None:
        filtered = True
        try:
            filtered_means, filtered_vars, _ = kalman_filter(data, data_innov_var, state_trans, state_innov_var,
                                                             data_mean, data_loadings, state_mean,
                                                             stationary=stationary,
                                                             check_correct_size=check_correct_size,
                                                             time_varying=time_varying)
        except AttributeError:
            raise AttributeError('If you do not specify filtered_means and filtered_vars you must specify parameters'
                                 'to calculate the kalman filtering algorithm.')

    time_dim, state_dim = filtered_means.shape

    if check_correct_size and filtered:
        if filtered_means.size != time_dim * state_dim or filtered_vars.size != time_dim * state_dim ** 2:
            raise ValueError("The dimensions of the parameters are not compatible.")

        if not time_varying:
            try:
                state_mean = (np.reshape(state_mean, state_dim) if state_mean is not None
                              else np.zeros(state_dim))
                state_trans = np.reshape(state_trans, (state_dim, state_dim))
                state_innov_var = np.reshape(state_innov_var, (state_dim, state_dim))
                state_mean = np.reshape(state_mean, state_dim)
            except ValueError:
                raise ValueError('The dimensions of the parameters are not compatible.')
        else:
            state_mean = (time_varying_reshape(state_mean, (time_dim, state_dim))
                          if state_mean is not None else np.zeros((time_dim, state_dim)))
            state_trans = time_varying_reshape(state_trans, (time_dim, state_dim, state_dim))
            state_innov_var = time_varying_reshape(state_innov_var, (time_dim, state_dim, state_dim))

    smoothing_func = ckalman_smoother_time_varying if time_varying else ckalman_smoother

    draws, smoothed_means, smoothed_vars = smoothing_func(state_trans=np.asfortranarray(state_trans),
                                                          state_innov_var=np.asfortranarray(state_innov_var),
                                                          state_mean=np.asfortranarray(state_mean),
                                                          filtered_means=np.asfortranarray(filtered_means),
                                                          filtered_covs=np.asfortranarray(filtered_vars))
    return draws, smoothed_means, smoothed_vars


def forecast_statespace(horizon, state_innov_var, state_trans, data_innov_var, data_loadings, final_state,
                        state_mean=None, data_mean=None):
    """
    Forecasts a statespace model for horizon steps.

    It simply rolls the state forward and then draws the noise variance.
    Parameters
    ----------
    horizon : positive int
    state_innov_var : 2d positive semi-definite ndarray
    state_trans : 2d square ndarray
    data_innov_var : 2d positive semi-definite ndarray
    data_loadings : 2d ndarray
    final_state : 1d ndarray
    state_mean : 1d ndarray
    data_mean : 1d ndarray

    Returns
    -------
    data : 2d ndarray
    states : 2d ndarray
    """
    state_mean = np.asarray(state_mean) if state_mean is not None else np.zeros(state_trans.shape[0])
    data_mean = np.atleast_2d(data_mean) if data_mean is not None else np.zeros(data_loadings.shape[0])
    data_loadings = np.atleast_2d(data_loadings)

    states = mcmc.simulate_var(time_dim=horizon, innov_var=state_innov_var, var_coeff=state_trans,
                               initial_state=final_state, mean=state_mean)
    data_innovations = stats.multivariate_normal.rvs(cov=data_innov_var, size=horizon)
    data = data_mean.T + states.dot(data_loadings.T) + data_innovations

    return data, states


def ensure_valid_shapes(shape, data_innov_var, data_loadings, state_trans, state_innov_var, data_mean, state_mean,
                        time_varying):
    """
    Simply ensures that the parameters have valid shapes.

    If time-varying is True, it will expand matrices or vectors to cubes and matrices so that the first dimension
    has the correct time dimension if necessary.

    Parameters
    ----------
    shape : tuple of positive ints
    data_innov_var : ndarray
    data_loadings : ndarray
    state_trans : ndarray
    state_innov_var : ndarray
    data_mean : ndarray
    state_mean : ndarray
    time_varying : Optional (bool)

    Returns
    -------
    data : ndarray
    data_innov_var : ndarray
    data_loadings : ndarray
    state_trans : ndarray
    state_innov_var : ndarray
    data_mean : ndarray
    state_mean : ndarray
    """
    time_dim, state_dim, data_dim = shape

    if time_varying:
        data_loadings_shape = (time_dim, data_dim, state_dim)
        state_trans_shape = (time_dim, state_dim, state_dim)
        data_mean_shape = (time_dim, data_dim)

        try:
            if not np.array_equal(data_loadings, None):
                data_loadings = time_varying_reshape(data_loadings, data_loadings_shape)
            elif state_dim == 1 or data_dim == 1:
                data_loadings = time_varying_reshape(np.ones((data_dim, state_dim)), data_loadings_shape)
            else:
                data_loadings = time_varying_reshape(np.eye(data_dim, state_dim), data_loadings_shape)

            data_mean = (time_varying_reshape(data_mean, data_mean_shape) if data_mean is not None
                         else np.zeros(data_mean_shape))

            state_innov_var = time_varying_reshape(state_innov_var, state_trans_shape)
            data_innov_var = time_varying_reshape(data_innov_var, (time_dim, data_dim, data_dim))
            state_mean = time_varying_reshape(state_mean if state_mean is not None
                                              else np.zeros(state_dim), (time_dim, state_dim))
            state_trans = time_varying_reshape(state_trans, state_trans_shape)

        except ValueError:
            raise ValueError('The parameters given have incompatible dimensions.')
    else:
        try:
            data_loadings_shape = (data_dim, state_dim)
            state_trans_shape = (state_dim, state_dim)
            if data_loadings is not None:
                data_loadings = np.reshape(data_loadings, data_loadings_shape)
            elif state_dim == 1 or data_dim == 1:
                data_loadings = np.ones(data_loadings_shape)
            else:
                data_loadings = np.eye(*data_loadings_shape)

            data_mean = np.reshape(data_mean, data_dim) if data_mean is not None else np.zeros(data_dim)

            state_innov_var = np.reshape(state_innov_var, state_trans_shape)
            data_innov_var = np.reshape(data_innov_var, (data_dim, data_dim))
            state_mean = (np.reshape(state_mean, state_dim) if state_mean is not None
                          else np.zeros(state_dim))
            state_trans = np.reshape(state_trans, state_trans_shape)
        except ValueError:
            raise ValueError('The parameters given have incompatible dimensions.')

    return data_innov_var, data_loadings, state_trans, state_innov_var, data_mean, state_mean
