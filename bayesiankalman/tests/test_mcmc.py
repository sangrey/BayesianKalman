import numpy as np
from numpy import linalg as nplin
import scipy.linalg as scilin
from bayesiankalman import mcmc
import bayesiankalman.kalmanfilter as kf
import pytest
from scipy import stats


# I want the same random values created every time.
np.random.seed(5345248)


@pytest.fixture(scope='module')
def dimensions():
    """
    Sets the dimensions of the statespace model

    Returns
    -------
    dim : tuple of positive ints
    """
    data_dim = 20
    time_dim = 50
    state_dim = 2

    return time_dim, state_dim, data_dim


@pytest.fixture(scope='module')
def simulate_kalman_model(dimensions):
    """
    Sigma_y += state_innov_var * np.diag(np.ones(dim - 1), k=1) + statmply simulates a standard state space model
    with gaussian innovations.

    Returns
    -------
    data : ndarray
    data_loadings : ndarray
    data_mean : ndarray
    data_innov_var : ndarray
    state_trans : ndarray
    state_trans : ndarray
    state_innov_var : ndarray

    """
    time_dim, state_dim, data_dim = dimensions
    data = np.asarray(np.random.standard_normal((time_dim, data_dim)))
    data_loadings = np.ones((state_dim, data_dim))
    data_loadings[0, 1] = .5
    data_mean = np.zeros(data_dim)
    data_innov_var = .3 * np.eye(data_dim)
    state_trans = .8 * np.eye(state_dim)
    state_trans[0, :] = 1
    state_innov_var = .3 * np.eye(state_dim)

    return data, data_loadings, data_mean, data_innov_var, state_trans, state_innov_var


# noinspection PyShadowingNames
def statespace_model(dimensions):
    """
   Simulates  a statespace model for use in testing kalman filtering, smoothing etcetera algorithms.

   Parameters
   ----------
   dimensions : tuple of positive ints

   Returns
   -------
   model : Kalman
   """
    time_dim, state_dim, data_dim = dimensions

    data_mean = np.random.standard_normal(data_dim)
    state_trans = np.random.standard_normal((state_dim, state_dim))
    data_loadings = np.random.standard_normal((data_dim, state_dim))
    state_innov_var = stats.invwishart.rvs(df=dimensions[1] + 20, scale=np.eye(state_dim))
    data_innov_var = stats.invwishart.rvs(df=dimensions[2] + 20, scale=np.eye(data_dim))
    state_mean = np.random.standard_normal(state_dim)

    model = kf.simulate_model(data_mean, state_trans, data_loadings, state_innov_var, data_innov_var,
                              state_mean=state_mean, time_dim=time_dim)

    return model


def test_create_lag_matrix():
    """ Ensures the function is creating the right lags for sequential data with 3 lags. """
    data = np.arange(20)
    lags = np.asarray([1, 2, 3])

    lag_mat = mcmc.create_lag_matrix(data, lags, prepend_ones=True)

    assert lag_mat.shape[0] == data.size - np.max(lags), 'The number of number of effective datapoints is not enough'
    assert lag_mat.shape[1] == lags.size + 1, 'The number of lags is not right.'
    assert lag_mat[len(lag_mat) // 2, 0] == 1, 'The first column is not ones.'
    assert np.array_equal(lag_mat[-1, lags], data[-(1 + lags)]), 'The columns do not have the right lag structure'


def test_create_lag_matrix_0_lags():
    """ Ensures that the function does the right thing when 0 lags are given. """

    data = np.arange(20)
    lags = 0
    external_regressors = np.arange(40).reshape((20, 2))

    lag_mat = mcmc.create_lag_matrix(data, lags, prepend_ones=False,
                                     external_regressors=external_regressors)
    assert np.allclose(lag_mat, external_regressors), "If 0 lags are given and  external_regressors is given, the " \
                                                      " function should return the external_regressors."
    lag_mat = mcmc.create_lag_matrix(data, lags, prepend_ones=False)

    assert np.allclose(lag_mat, []), "If 0 lags are given, prepend_ones=False, and external_regressors=None, " \
                                     "The lag_matrix should be empty."

    lag_mat = mcmc.create_lag_matrix(data, lags)

    assert np.allclose(lag_mat, np.ones(data.size)), "If 0 lags are given, it should return a vector of just ones."


def test_create_lag_matrix_multiple_matrices():
    """ Ensures that the function does the same thing when you feed in list of tuples.  """

    data = np.arange(20)
    lags = [1, 2, 3]
    lag_mat1 = mcmc.create_lag_matrix(data, lags)
    lag_mat2 = mcmc.create_lag_matrix([(data, lags)])

    lag_mat3 = mcmc.create_lag_matrix([(data, lags), (data, lags)])

    assert np.allclose(lag_mat1, lag_mat2), "The two different ways to call the function must give identical results."

    assert np.allclose(lag_mat1[:, 1:], lag_mat3[:, -len(lags):]), "The second method of calling the function should " \
                                                                   "work with iterable of (data, lags) pairs."


def test_kalman_filter_no_dynamics(dimensions):
    """
   This tests that the kalman filter does the right thing when there are no dynamics and so we can analytically
   solve for the right answer.
       """
    state_dim = 1
    data_dim = 1
    time_dim = dimensions[0]
    state_innov_var = 1
    data_innov_var = 1

    model = kf.simulate_model(state_trans=np.zeros(state_dim), data_loadings=np.ones(data_dim),
                              state_innov_var=state_innov_var,
                              data_innov_var=data_innov_var,
                              time_dim=time_dim,
                              data_mean=np.zeros(data_dim))
    means, cov, _ = kf.kalman_filter(state_trans=0, state_innov_var=state_innov_var,
                                     data=model.data, data_innov_var=data_innov_var,
                                     data_loadings=1,
                                     stationary=True, time_varying=False)
    variance = (1 / state_innov_var + 1 / data_innov_var) ** (-1)
    mean = model.data / data_innov_var * variance

    assert np.allclose(cov, variance), "The variance is not right when there are no dynamics."
    assert np.allclose(mean, means), "The means are not right when there are no dynamics."


def test_kalman_filter_time_varying(dimensions):
    """
   Ensures that the time-varying estimation and fixed estimations are identical if the parameters are fixed.

   Parameters
   ----------
   dimensions : tuple of positive ints
   """
    time_dim, state_dim, data_dim = dimensions

    model = statespace_model(dimensions)
    state_trans = mcmc.make_stationary(.9 * model.state_trans)
    data = model.data
    state_innov_var = model.state_innov_var
    data_mean = model.data_mean
    data_loadings = model.data_loadings
    data_innov_var = model.data_innov_var
    draws1, _, _ = kf.kalman_filter(state_trans=state_trans, state_innov_var=state_innov_var,
                                    data=data, data_innov_var=data_innov_var,
                                    data_mean=data_mean, data_loadings=data_loadings,
                                    stationary=False, time_varying=False)

    draws2, _, _ = kf.kalman_filter(state_trans=state_trans, state_innov_var=state_innov_var,
                                    data=data, data_innov_var=data_innov_var,
                                    data_mean=data_mean, data_loadings=data_loadings,
                                    stationary=False, time_varying=True)
    broadcasted_state_innov_var = np.ascontiguousarray(
        np.broadcast_to(model.state_innov_var, shape=(time_dim, state_dim, state_dim)))
    draws3, _, _ = kf.kalman_filter(state_trans=state_trans, state_innov_var=broadcasted_state_innov_var,
                                    data=data, data_innov_var=data_innov_var,
                                    data_mean=data_mean, data_loadings=data_loadings,
                                    stationary=False, time_varying=True)

    assert np.allclose(draws1, draws2), 'The time-varying and regular estimation procedures are giving different' \
                                        ' results.'

    assert np.allclose(draws1, draws3), 'The model with a broadcasted parameter does not equal the regular estimate.'


def test_kalman_filter_heteroskedastic_no_dynamics(dimensions):
    """ This is a similar test to the one above, but I let the data have a heteroskedastic noise term. """
    time_dim = dimensions[0]
    states = np.random.standard_normal(time_dim)
    data_innov_var = np.exp(-2 * np.abs(np.random.standard_normal(time_dim)))
    data = states + np.sqrt(data_innov_var) * np.random.standard_normal(time_dim)
    state_innov_var = 1
    data_innov_var = 1

    means, cov, _ = kf.kalman_filter(state_trans=0, state_innov_var=state_innov_var,
                                     data=data, data_innov_var=data_innov_var,
                                     data_loadings=1,
                                     stationary=True, time_varying=False)

    variances = (1 / state_innov_var + 1 / data_innov_var) ** (-1)
    other_means = np.ravel(data) / data_innov_var * variances

    assert np.allclose(cov, variances), 'The variances are not the same in the heteroskedastic case.'
    assert np.allclose(np.ravel(means), other_means), 'The means are not the same. '


def test_kalman_smoother_no_dynamics(dimensions):
    """
    This tests that the kalman filter does the right thing when there are no dynamics and so we can analytically
    solve for the right answer.
    """
    state_dim = 1
    data_dim = 1
    time_dim = dimensions[0]
    state_innov_var = 1
    data_innov_var = 1

    model = kf.simulate_model(state_trans=np.zeros(state_dim), data_loadings=np.ones(data_dim),
                              state_innov_var=state_innov_var,
                              data_innov_var=data_innov_var,
                              time_dim=time_dim,
                              data_mean=np.zeros(data_dim))
    _, means, cov = kf.kalman_smoother(state_trans=0, state_innov_var=state_innov_var,
                                       data=model.data, data_innov_var=data_innov_var,
                                       data_loadings=1,
                                       stationary=True, time_varying=False)
    variance = (1 / state_innov_var + 1 / data_innov_var) ** (-1)
    mean = model.data / data_innov_var * variance

    assert np.allclose(cov, variance), "The variance is not right when there are no dynamics."
    assert np.allclose(mean, means), "The means are not right when there are no dynamics."


def test_kalman_smoother_heteroskedastic_no_dynamics(dimensions):
    """
   This is a similar test to the one above, but I let the data have a heteroskedastic noise term.
   """
    time_dim = dimensions[0]
    states = np.random.standard_normal(time_dim)
    data_innov_var = np.exp(-2 * np.abs(np.random.standard_normal(time_dim)))
    data = states + np.sqrt(data_innov_var) * np.random.standard_normal(time_dim)
    state_innov_var = 1
    data_innov_var = 1

    _, means, cov = kf.kalman_smoother(state_trans=0, state_innov_var=state_innov_var,
                                       data=data, data_innov_var=data_innov_var,
                                       data_loadings=1,
                                       stationary=True, time_varying=False)

    variances = (1 / state_innov_var + 1 / data_innov_var) ** (-1)
    other_means = np.ravel(data) / data_innov_var * variances

    assert np.allclose(cov, variances), 'The variances are not the same in the heteroskedastic case.'
    assert np.allclose(np.ravel(means), other_means), 'The means are not the same. '


def test_kalman_filter_same_mean(dimensions):
    """ This test ensures that the filter does the right thing when we feed it in just a mean. """
    time_dim, state_dim, data_dim = dimensions

    state_innov_var = np.eye(state_dim)
    data_innov_var = np.eye(data_dim)
    state_trans = .9 * np.eye(state_dim)
    data_mean = 0.0

    means, cov, _ = kf.kalman_filter(state_trans=state_trans,
                                     state_innov_var=state_innov_var,
                                     data_mean=np.full(data_dim, data_mean),
                                     data=np.full((time_dim, data_dim), data_mean),
                                     data_innov_var=data_innov_var,
                                     data_loadings=np.eye(data_dim, state_dim),
                                     stationary=True,
                                     time_varying=False)

    assert np.allclose(np.zeros((time_dim, state_dim)), means), \
        "The means are changing when I observe the mean in every period."


def test_smoother_iid_states(dimensions):

    time_dim = dimensions[0]
    state_dim = 2 
    data_dim = 1
    state_innov_var = 1
    data_innov_var = 1
    state_trans = np.asarray([[0, 0], [1, 0]])

    for dim in range(2, time_dim):
        sigma_y = (2 * state_innov_var + data_innov_var) * np.eye(dim)
        sigma_y += state_innov_var * np.diag(np.ones(dim - 1), k=1) + state_innov_var * np.diag(np.ones(dim - 1), k=-1)
        sigma_x = state_innov_var * np.eye(dim)
        sigma_xy = (sigma_x + state_innov_var * np.diag(np.ones(dim - 1), 1))
        sigma = np.vstack([np.hstack([sigma_x, sigma_xy]), np.hstack([sigma_xy.T, sigma_y])])
        posterior_state_var = (sigma[:dim, :dim] - sigma[dim:, :dim] @
                               np.linalg.solve(sigma[dim:, dim:], sigma[dim:, :dim].T))

        _, _, cov = kf.kalman_smoother(state_trans=state_trans,
                                       state_innov_var=np.asarray([[state_innov_var, 0], [0, 0]]),
                                       data=np.full((dim, data_dim), 1.0),
                                       data_innov_var=data_innov_var,
                                       data_loadings=np.ones((data_dim, state_dim)),
                                       stationary=True,
                                       time_varying=False)

        filter_err_msg = "The variance of the final element is not right when the states are iid." \
                         " I.e. the filter does not work"

        smoother_err_msg = "The smoother is not giving the right conditional variances. That is the smoother " \
                           "is not working. I am currently at dim= " + str(dim)

        assert np.allclose(cov[-1, -1, -1], posterior_state_var[-1, -1]), filter_err_msg
        assert np.allclose(cov[-1, -1, -1], posterior_state_var[-1, -1]), filter_err_msg

        for idx in range(1, dim):
            posterior_cond_var = (posterior_state_var[:idx, :idx] - posterior_state_var[:idx, idx:] @
                                  np.linalg.solve(posterior_state_var[idx:, idx:], posterior_state_var[idx:, :idx]))
            assert np.isclose(cov[idx - 1, -1, -1], posterior_cond_var[-1, -1]), (smoother_err_msg + "The index is" +
                                                                                  str(idx))


def test_matrix_coeff_moments():
    """ Ensures that the draw_matrix_coefficient is giving the correct values for a subset of inputs. """
    prior_mean = np.zeros(1)
    prior_var = 1
    innovation_var = 1
    regressand = np.ones(2)
    regressor = np.ones(2)
    trans_precision_factored, mean = mcmc.matrix_coeff_moments(prior_mean=prior_mean, prior_var=prior_var,
                                                               regressand=regressand, regressor=regressor,
                                                               innovation_var=innovation_var)
    assert np.allclose(mean, 2 / 3)

    trans_precision_factored, mean = mcmc.matrix_coeff_moments(regressand=regressand, regressor=regressor,
                                                               innovation_var=innovation_var, prior_mean=0, prior_var=0)

    assert np.allclose(mean, 1)

    regressor = np.random.standard_normal((100, 5))
    regressand = np.sum(regressor, axis=1) + innovation_var * np.random.standard_normal(100)
    ols_est = mcmc.ols(regressor=regressor, regressand=regressand)
    ols_var = innovation_var * scilin.pinv(regressor.T @ regressor)
    trans_precision_factored, mean = mcmc.matrix_coeff_moments(regressand=regressand, regressor=regressor,
                                                               innovation_var=innovation_var, prior_mean=0, prior_var=0)
    assert np.allclose(mean, ols_est)

    assert np.allclose(scilin.cho_solve((trans_precision_factored, False), np.eye(ols_var.shape[0])), ols_var)


def test_matrix_coeff_moments_heteroskedastic_simple():
    """ Ensures the heteroskedastic path is giving right answers for a simple problem. """

    prior_mean = np.zeros(1)
    prior_var = 1
    time_dim = 2
    innovation_var = np.ones((time_dim, 1))
    regressand = np.ones(time_dim)
    regressor = np.ones(time_dim)
    trans_precision_factored, mean = mcmc.matrix_coeff_moments(prior_mean, prior_var, regressand, regressor,
                                                               innovation_var)
    assert np.allclose(mean, 2 / 3)


def test_matrix_coeff_moments_heteroskedastic(dimensions):
    """ Ensures the matrix is giving the same output when the heteroskedatic path is chosen as when it is not."""

    time_dim, state_dim, data_dim = dimensions
    
    prior_mean = np.zeros((state_dim, data_dim))
    prior_var = np.zeros((data_dim, data_dim))
    innovation_var = np.diag(np.arange(1, data_dim + 1))
    coefficient = np.random.standard_normal((state_dim, data_dim))
    regressor = np.random.standard_normal((time_dim, state_dim))

    regressand = (np.atleast_2d(regressor) @ np.atleast_2d(coefficient) +
                  np.random.standard_normal((time_dim, data_dim)).dot(innovation_var))
    reshaped_innovation_var = np.tile(innovation_var, (time_dim, 1, 1))

    normal_precision_factored, normal_mean = mcmc.matrix_coeff_moments(regressand=regressand, regressor=regressor,
                                                                       innovation_var=innovation_var,
                                                                       prior_mean=prior_mean,
                                                                       prior_var=prior_var)

    hetero_precision_factored, hetero_mean = mcmc.matrix_coeff_moments(regressand=regressand, regressor=regressor,
                                                                       innovation_var=reshaped_innovation_var,
                                                                       prior_mean=prior_mean, prior_var=prior_var)

    assert np.allclose(normal_mean, hetero_mean), "The mean from the hetero path is not the same " \
                                                  "results as the standard path when the elements are not correlated " \
                                                  "in each period.."

    assert np.allclose(normal_precision_factored, hetero_precision_factored), "The factored precision from the " \
                                                                              "hetero path is not same as from the " \
                                                                              "standard path when the data are not " \
                                                                              "correlated in each period.."

    innovation_var = stats.invwishart.rvs(df=20, scale=np.eye(data_dim))
    reshaped_innovation_var = np.tile(innovation_var, (time_dim, 1, 1))
    normal_precision_factored, normal_mean = mcmc.matrix_coeff_moments(regressand=regressand, regressor=regressor,
                                                                       innovation_var=innovation_var,
                                                                       prior_mean=prior_mean,
                                                                       prior_var=prior_var)

    hetero_precision_factored, hetero_mean = mcmc.matrix_coeff_moments(regressand=regressand, regressor=regressor,
                                                                       innovation_var=reshaped_innovation_var,
                                                                       prior_mean=prior_mean, prior_var=prior_var)

    assert np.allclose(normal_mean, hetero_mean), "The mean from the hetero path is not the same " \
                                                  "results as the standard path when the elements are correlated " \
                                                  "in each period.."

    assert np.allclose(normal_precision_factored, hetero_precision_factored), "The factored precision from the " \
                                                                              "hetero path is not same as from the " \
                                                                              "standard path when the data are " \
                                                                              "correlated in each period.."


# noinspection PyShadowingNames
def test_simulate_var(dimensions):
    """
    Ensures that the data is being simulated from the correct model.
    The test itself comes from since we know the true parameters and the data generating process is normal, if
    get the residuals, orthogonalize, and square them, we have a square of (data_dim * time_dim) independent normal
    random variables, and so we have a valid hypothesis test. Furthermore, we can get this result without any asymptotic
    approximations.

    Parameters
    ----------
    dimensions : tuple of positive ints.

    """

    time_dim, data_dim, _ = dimensions
    time_dim *= 10
    var_coeff = mcmc.make_stationary(np.random.standard_normal((data_dim, data_dim)))
    innov_var = stats.invwishart.rvs(df=data_dim + 20, scale=np.eye(data_dim))
    initial_state = np.random.standard_normal(data_dim)
    mean = np.random.standard_normal(data_dim)

    data = mcmc.simulate_var(time_dim, innov_var, var_coeff, initial_state, mean)

    deviations = data[1:] - (data[:-1].dot(var_coeff.T) + mean)

    root_innov_var = nplin.cholesky(np.atleast_2d(innov_var))

    stat = np.sum(nplin.solve(root_innov_var, deviations.T) ** 2)

    assert stat < stats.chi2.ppf(q=.999, df=time_dim * data_dim)


def test_time_varying_reshape():
    """
    Ensures that the matrix is being appropriately broadcast to a cube, and nothing is being done to a cube.

    It also
    """
    try_matrix = np.arange(16).reshape((4, 4))
    time_dim = 8

    assert np.allclose(mcmc.time_varying_reshape(try_matrix, (time_dim,) + try_matrix.shape)[0], try_matrix), \
        'The matrix is not create a 3d array with time_dim copies of itself.'

    try_cube = np.arange(24).reshape((2, 3, 4))

    assert np.allclose(mcmc.time_varying_reshape(try_cube, try_cube.shape), try_cube), 'A cube should not change.'


def test_normal_log_density():
    """
   Ensures that my log normal function is returning the same as scipy.stat's version.
   """
    vals = np.random.standard_normal(10)
    means = np.random.standard_normal(10)
    scales = np.abs(np.random.standard_normal(10))

    for val, mean, scale in zip(vals, means, scales):
        assert np.isclose(stats.norm.logpdf(val, mean, scale), mcmc.normal_log_density(val, mean, scale))


def test_multivariate_normal_log_density():
    """
   Ensures that the log normal function is giving the correct output in the multivariate case.
   """
    dim = 10
    num_vals = 10
    vals = np.random.standard_normal((num_vals, dim))
    means = np.random.standard_normal((num_vals, dim))
    covariances = stats.invwishart.rvs(df=dim + 5, scale=np.eye(dim), size=num_vals)

    for val, mean, cov in zip(vals, means, covariances):
        assert np.isclose(stats.multivariate_normal.logpdf(val, mean, cov), mcmc.normal_log_density(val, mean,
                                                                                                    covariance=cov))


def test_draw_innovation_var():
    prior_scale, prior_shape = 0, 0

    regressor = np.random.standard_normal(100)
    coeff = 1
    assert mcmc.draw_innovation_var(regressor, regressor, coeff, prior_scale, prior_shape) == 0, \
        ' You should return zero if the prior scale and variance are 0, and you can match the data exactly. '


def test_ols():
    regressor = np.random.standard_normal((100, 2))
    regressand = np.random.standard_normal(100)

    assert np.allclose(mcmc.ols(regressor, regressand), 
                       scilin.pinv(regressor.T @ regressor) @ regressor.T @ regressand), \
        'The least squares solution should give the same value as the direct solution.'


def test_frac_diff():
    data = np.random.standard_normal(2000)

    assert np.allclose(mcmc.frac_diff(data, d=1, truncation_order=1), data[1:] - data[:-1]), \
        'The data is not being differences when d=1'

    assert np.allclose(mcmc.frac_diff(data, d=0, truncation_order=1), data[1:]), 'If d=0, it should not do anything'


def switch_storage_order_variance_matrix():
    """ Ensures the storage order is actually being switched.  """

    nrows = 5
    ncols = 5

    a_mat = np.arange(nrows * ncols)

    reordered_matrix = switch_storage_order_variance(np.outer(a_mat.T, a_mat.T), nrows, ncols)
    assert np.allclose(np.outer(a_mat, a_mat), reordered_matrix), \
            " The extract_matrix function is not reordering the indices correctly."
    nrows = 5
    ncols = 5

    a_mat = np.arange(nrows * ncols)

    reordered_matrix = switch_storage_order_variance(np.outer(a_mat.T, a_mat.T), nrows, ncols)
    assert np.allclose(np.outer(a_mat, a_mat), reordered_matrix), \
            " The extract_matrix function does not work on non-square matrices."
