//
// Created by sangrey on 2/17/16. See kalmanfilter.h function for comments.
//

#include <pybind11/pybind11.h> 
#include <pybind11/numpy.h> 
#include <arma_wrapper.h> 
#include <kalmanfilter.h>
#include <iostream>


namespace py = pybind11; using namespace pybind11::literals;


using arma::Mat; using arma::Cube; using aw::py_arr; using kal::npint; using kal::npdouble; using kal::npulong;
using kal::intcube; using kal::dcube; using kal::intmat; using kal::dmat; using kal::dvec; 




dmat simulate_var(const dmat& var_coeff, const dmat& innov_var, size_t time_dim, const dmat& initial_state, const
        dmat& mean) { 

    return kal::simulate_var(var_coeff, innov_var, time_dim, initial_state, mean); 

}

auto kalman_filter(const dmat& data, const dmat& data_innov_var, const dmat& state_trans, const dmat&
        state_innov_var, const dvec& data_mean, const dmat& data_loadings, const dvec& state_mean, bool
        stationary) { 

     
    return kal::kalman_filter(data, data_innov_var, state_trans, state_innov_var, data_mean, data_loadings,
            state_mean, stationary);


}    


auto kalman_filter_time_varying(const dmat& data, const dcube& data_innov_var, const dcube& state_trans, const
        dcube& state_innov_var, const dmat& data_mean, const dcube& data_loadings, const dmat& state_mean, bool
        stationary) {

    
    return kal::kalman_filter(data, data_innov_var, state_trans, state_innov_var, data_mean,  data_loadings,
            state_mean,stationary);

} 

auto kalman_smoother(const dmat& state_trans, const dmat& state_innov_var, const dvec& state_mean, const dmat&
        filtered_means, const dcube& filtered_covs) {

    return kal::kalman_smoother(state_trans, state_innov_var, state_mean, filtered_means, filtered_covs); 

}


auto kalman_smoother_time_varying(const dcube& state_trans, const dcube& state_innov_var, const dmat& state_mean,
        const dmat& filtered_means, const dcube& filtered_covs) {


    return kal::kalman_smoother(state_trans, state_innov_var, state_mean, filtered_means, filtered_covs); }


npdouble scalar_normal_log_density(npdouble x, npdouble mean, npdouble scale) {

    npdouble variance = scale * scale; return -.5 * (kal::log2pi + std::log(variance) + ((x - mean) * (x-mean) /
                (variance))); }


npdouble multivariate_normal_log_density(dmat x, dmat mean, dmat covariance) { return
    kal::multivariate_normal_log_density(dvec(x), dvec(mean), covariance);

} 


std::tuple<dmat, dmat> matrix_coeff_moments(const dmat& regressor, const dmat& regressand, const dmat&
        innovation_var, const dmat& prior_mean, const dmat& prior_var) {


     return kal::matrix_coeff_moments(regressor, regressand, innovation_var, prior_mean, prior_var); }  


std::tuple<dmat, dmat> matrix_coeff_moments_no_prior(const dmat& regressor, const dmat& regressand, const dmat&
        innovation_var) {

    return kal::matrix_coeff_moments(regressor, regressand, innovation_var); }  


dmat switch_storage_order_variance_matrix(const dmat& vec_outer_prod, npulong  n_cols, npulong n_rows) { return
    kal::switch_storage_order_variance_matrix(vec_outer_prod, n_cols, n_rows); }


std::tuple<dmat, dmat> matrix_coeff_moments_hetero(const dmat& regressor, const dmat& regressand, const dcube&
        innovation_var, const dmat& prior_mean, const dmat& prior_var) {

    std::tuple<dmat, dmat> return_tup;
    
    { py::gil_scoped_release release; return_tup = kal::matrix_coeff_moments(regressor, regressand,
            innovation_var,prior_mean, prior_var);

    }

    return return_tup; }


std::tuple<dmat,dmat> matrix_coeff_moments_hetero_no_prior(const dmat& regressor, const dmat& regressand, const
        dcube& innovation_var) { return kal::matrix_coeff_moments(regressor, regressand, innovation_var); 

}


PYBIND11_MODULE(bayesiankalman_ext, m) {                                                                                         
    
    m.def("csimulate_var",&simulate_var, "The c++ implementation to simulate a Vector  Autoregression.",
            "var_coeff"_a, "innov_var"_a, "time_dim"_a, "initial_state"_a, "mean"_a);

    m.def("ckalman_filter", &kalman_filter, "The C++ implementation of the Kalman filtering algorithm.", "data"_a,
            "data_innov_var"_a, "state_trans"_a, "state_innov_var"_a, "data_mean"_a, "data_loadings"_a,
            "state_mean"_a, "stationary"_a);

    m.def("ckalman_filter_time_varying", &kalman_filter_time_varying, "A C++ implementation of the Kalman "
            "algorithm that allows for time-varying parameters.", 
            "The C++ implementation of the Kalman filtering algorithm with time varying parameters.", "data"_a,
            "data_innov_var"_a, "state_trans"_a, "state_innov_var"_a, "data_mean"_a, "data_loadings"_a,
            "state_mean"_a, "stationary"_a);

    m.def("ckalman_smoother", &kalman_smoother, "A C++ implementation of the Kalman smoother. It assumes " 
            "that the kalman filter has already been run.", "state_trans"_a, "state_innov_var"_a, "state_mean"_a,
            "filtered_means"_a, "filtered_covs"_a); 
   
    m.def("ckalman_smoother_time_varying", &kalman_smoother_time_varying,
            "A C++ implementation of the Kalman smoother. It assumes that the kalman filter has already been " 
            "run.", "state_trans"_a, "state_innov_var"_a, "state_mean"_a, "filtered_means"_a, "filtered_covs"_a); 
    
    m.def("cscalar_normal_log_density", &scalar_normal_log_density, "The normal log density.", "x"_a, "mean"_a,
            "scale"_a);
    
    m.def("cmultivariate_normal_log_density", &multivariate_normal_log_density, 
            "The multivariate normal log density.", "x"_a, "mean"_a, "covariance"_a);

    m.def("cmatrix_coeff_moments", &matrix_coeff_moments, "Computes the posterior moments of a matrix normal "
            "distribution", "regressor"_a, "regressand"_a, "innovation_var"_a, "prior_mean"_a, "prior_var"_a);


    m.def("cmatrix_coeff_moments_no_prior", &matrix_coeff_moments_no_prior, "Compute the posterior moments of a "
            "matrix normal distribution with a flat prior.", "regressor"_a, "regressand"_a, "innovation_var"_a);
    
    m.def("switch_storage_order_variance", &switch_storage_order_variance_matrix, "Switches the storage order "
            "of a variance matrix.", "vec_outer_prod"_a, "nrows"_a, "ncols"_a);

    m.def("cmatrix_coeff_moments_hetero", &matrix_coeff_moments_hetero, "Computes the posterior" 
            "moments of a matrix normal random variable.", "regressor"_a, "regressand"_a, "innovation_var"_a,
            "prior_mean"_a, "prior_var"_a); 
    
    m.def("cmatrix_coeff_moments_hetero_no_prior", &matrix_coeff_moments_hetero_no_prior, 
            "Computes the moments of the matrix normal distribution when there is no prior and the variance is "
            "heteroskedastic.", "regressor"_a, "regressand"_a, "innovation_var"_a); 
    
}                                                                                                                       
                                                                                                                  


