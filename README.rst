This repository aims to be a well-tested, fast implementation of the
multi-move gibbs sampler of Carter and Kohn (1993). 
The main part of the code depends upon scipy and
numpy. 


This repository provides functions for Kalman Filtering and and Kalman smoothing,
which provide draws of the states as well as the the mean and variance of the
states. It also provides methods for drawing the parameters using a gibbs
sampler with conjugate priors.

I allow for arbitrary time-varying parameter matrices, which allows you 
to do heteroskedasticity in a general way. I also allow for degenerate
innovation variance matrices which allow you to estimate MA(k) models.

Beyond that I have several helper functions for simulating and saving
results while doing Bayesian estimation.  For example, I can simulate 
a general vector autorgression, and have methods that use pytables
to recusrively save parameters from rather arbitrary samplers.
These methods are not as comprehensively tested as the filtering 
and smoothing algorithms are though.

It builds a Python package that can be downloaded from https://anaconda.org/sangrey/bayesiankalman using conda.
