\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{thmtools}
\usepackage{thm-restate}
\usepackage{cleveref}
\usepackage{etex, etoolbox, ifthen, url,csquotes}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, autopunct=true, authordate]{biblatex-chicago}
\usepackage{placeins}
\usepackage[page]{appendix}
\usepackage{siunitx}
\usepackage[T1]{fontenc}
\usepackage{csquotes}



\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}{Lemma}
\pagestyle{plain}

\newcommand{\E}{\mathbb{E}}
\newcommand{\Var}{\mathbb{V}\mathrm{ar}}
\newcommand{\Cov}{\mathbb{C}\mathrm{ov}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\lequals}{\stackrel{\mathcal{L}}{=}}
\newcommand{\aequals}{\stackrel{a.s.}{=}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\qt}[1]{\lq\lq#1\rq\rq}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\vect}{vec}

\begin{document}

In this paper I derive the posterior moments for a matrix-valued $\beta$ implied by the following model. 
There are $T$ periods, $P$ regressors and $K$ observed variables.  

$$ Y_t = \beta' X_t + H_t, H_t \stackrel{i.i.d}{\sim} \N(0,\Sigma_K)$$


\section{Derivation of Posterior for $Y = X \beta + H$ with flat prior}

Consider the following model. 
First, we start by deriving the posterior when there is no prior. 
Note, $Y$ is $T \times K$, $X$ is $T \times P$, and $\beta$ is $P \times K$.
Then the posterior distribution of $\beta$ is proportional to the following. 

\begin{align*}
& \exp\left\lbrace -\frac{1}{2} \tr\left[\sum_{t=1}^T (Y_t' - \beta' X_t')' \Sigma^{-1} (Y_t' - \beta' X_t') \right] \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \sum_{t=1}^T \tr\left[(Y_t' - \beta' X_t') (Y_t' - \beta' X_t')' \Sigma^{-1}  \right] \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \sum_{t=1}^T \tr\left[(\Sigma^{-1})' (Y_t - X_t \beta)' (Y_t - X_t \beta) \right] \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})'  \sum_{t=1}^T (Y_t - X_t \beta)' (Y_t - X_t \beta) \right] \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' (Y - X \beta)' (Y - X \beta) \right] \right\rbrace \\
&\propto \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' \left(-\beta' X' Y -Y' X \beta + \beta' X' X \beta \right)  \right] \right\rbrace \\
&\propto \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' \left(-\beta' X' Y -Y' X \beta + \beta' X' X \beta \right)  \right] \right\rbrace \\
&\propto \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' \left(-\beta' X' Y -Y' X \beta + \beta' X' X \beta + Y'X (X'X)^{-1} X' Y\right)  \right] \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' \left(\beta - (X'X)^{-1}X'Y\right)' (X'X) \left(\beta - (X'X)^{-1}X'Y \right) \right] \right\rbrace \\
\end{align*}

Consequently, we have that $\beta \sim \mathcal{M}\N\left((X'X)^{-1}X'Y, (X'X)^{-1}, \Sigma^{-1} \right)$. 

\section{Derivation with Prior} 

If we have a prior such that the prior distribution for $\beta$ is i.i.d across the the regressors, 
that is each column of $\beta$ is distributed $\N(\mu_{\beta}, \Sigma_{\beta})$.


Then the posterior distribution of $\beta$ is proportional to the following.
Let $\hat{\beta} = (X'X)^{-1} X'Y$. 

\begin{align*}
& \exp\left\lbrace -\frac{1}{2} \tr\left[\sum_{t=1}^T (Y_t' - \beta' X_t')' \Sigma^{-1} (Y_t' - \beta' X_t') \right] \right\rbrace \cdot \exp\left\lbrace -\frac{1}{2} \tr\left[\Sigma_{\beta}^{-1} (\beta - \mu_{\beta})'  (\beta - \mu_{\beta}) \right] \right\rbrace\\
&\propto \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' \left(\beta - \hat{\beta}\right)' (X'X) \left(\beta - \hat{\beta}\right) \right] \right\rbrace  \cdot \exp\left\lbrace -\frac{1}{2} \tr\left[\Sigma_{\beta}^{-1} (\beta - \mu_{\beta})'  (\beta - \mu_{\beta}) \right] \right\rbrace\\
&= \exp\left\lbrace -\frac{1}{2} \tr\left[(\Sigma^{-1})' \left(\beta - \hat{\beta}\right)' (X'X) \left(\beta - \hat{\beta}\right) + \Sigma_{\beta}^{-1} (\beta - \mu_{\beta})'  (\beta - \mu_{\beta}) \right] \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \vect\left(\beta - \hat{\beta}\right)' \left((\Sigma^{-1})' \otimes (X'X)\right)\vect\left(\beta - \hat{\beta}\right) + \vect(\beta - \mu_{\beta})' \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right) \vect(\beta - \mu_{\beta}) \right\rbrace \\
&= \exp\left\lbrace -\frac{1}{2} \left[ \tr\left[ \left( \left((\Sigma^{-1})' \otimes (X'X)\right) + \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right) \right) \vect(\beta) \vect(\beta)' \right] \right.\right. \\
&\left.\left.\hspace{10mm} - 2 \tr \left[ \left(\left((\Sigma^{-1})' \otimes (X'X)\right) \vect(\hat{\beta}) + \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right)  \vect(\mu_{\beta}) \right)' \vect(\beta) \right] \right] \right\rbrace \\
&\propto \exp\left\lbrace -\frac{1}{2} \left[ \left(\vect(\beta) - \Var(\beta) \left(\left((\Sigma^{-1})' \otimes (X'X)\right) \vect(\hat{\beta}) + \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right)  \vect(\mu_{\beta}) \right)\right)' \right.\right. \\
& \left.\left.\hspace{10mm}  \Var(\beta)^{-1} \left(\vect(\beta) - \Var(\beta) \left(\left((\Sigma^{-1})' \otimes (X'X)\right) \vect(\hat{\beta}) + \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right)  \vect(\mu_{\beta}) \right)\right) \right]\right\rbrace 
\end{align*} 


Consequently, we have the following. 

\begin{gather*}
\vect(\beta) \sim \N\left(\Var(\beta) \left(\left((\Sigma^{-1})' \otimes (X'X)\right) \vect(\hat{\beta}) + \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right)  \vect(\mu_{\beta}) \right), \right. \\
 \left. \Var(\beta) =  \left( \left((\Sigma^{-1})' \otimes (X'X)\right) + \left(\Sigma_{\beta}^{-1} \otimes \I_{P} \right) \right)^{-1} \right)
\end{gather*}

\end{document}


