\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{etex}
\usepackage{etoolbox}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage{csquotes}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, authordate]{biblatex-chicago}
\usepackage{placeins}


\begin{document}


\section{Introduction}
 
 In the notes below we derive a Kalman Filter and Kalman Smoother in the case where the contemporaneous shocks are independent and identically-distributed.  If one wanted to it in more generality, that would not be too difficult. You can follow the steps below as a guide.
 
The key to the derivation is to use properties and conditional normality and exploit the recursive structure of the problem. 


The model we assume has the following structure. 

\begin{align*}
\Sigma_t = \mu + Z' \Xi_t + \sigma_{\eta} H_t \\
\Xi_t = \Phi \Xi_{t-1} + \sigma_{\xi} U_t \\
\end{align*}



\section{Kalman Filter}


We use $\Omega_t$ to refer to the information set at time $t$. 

$$\left.\begin{pmatrix} \Sigma_t \\ \Xi_t \end{pmatrix}\right\rvert \Omega_{t-1}  \sim
 \mathcal{N} \left( \begin{matrix} \mu + Z' \Phi \Xi_{t-1} \\ \Phi \Xi_{t-1} \end{matrix}, 
 \begin{pmatrix} \sigma^2_{\xi} Z'Z + \sigma^2_{\eta}\mathbb{I}_K & \sigma^2_{\xi}Z' \\ \sigma^2_{\xi}Z & \sigma^2_{\xi}\mathbb{I}_K \\ \end{pmatrix}  \right) $$ 

% Todo: I need to work out a more general formula. 

We can therefore use the properties of normal distributions to define the distribution of $\Xi_t \vert \Omega_t$ 


$$\Xi_t \vert \Omega_t \sim \mathcal{N} \left( \Phi \mathbb{E}_t[\Xi_{t-1}] + \frac{\sigma^2_{\xi}}{\sigma^2_{\xi} + \sigma^2_{\eta}} (\Sigma_t - \mu - \Phi \mathbb{E}_t[\Xi_{t-1}]), \left( \sigma^2_{\xi} - \frac{\sigma^4_{\xi}}{\sigma^2_{\xi} + \sigma^2_{\eta}} \right) \mathbb{I}_K \right)$$


Equivalently, we have the following. 

$$\Xi_{t} \vert \Omega_t \sim \mathcal{N} \left( \left(1 - \frac{\sigma^2_{\xi}}{\sigma^2_{\xi} + \sigma^2_{\eta}} \right) \Phi \mathbb{E}_t[\Xi_{t-1}] + \frac{\sigma^2_{\xi}}{\sigma^2_{\xi} + \sigma^2_{\eta}}  (\Sigma_t - \mu) , \sigma^2_{\xi} \left(1 - \frac{\sigma^2_{\xi}}{\sigma^2_{\xi} + \sigma^2_{\eta}} \right) \mathbb{I}_k  \right) $$


\section{Kalman Smoother}

If we simply want to estimate the factor and aren't worried about an online algorithm, then we would want to use all the information including values in the future. 

Let $\mu_t$ be $\mathbb{E}[\Sigma_t | \Omega t]$, and $\tau_t$ be the coefficient in front of the idenity in $\mbox{Var}[\Sigma_t | \omega_t ]$. 
In other words, the values in the expression right above. 

Using that notation we can derive the distribution of state in the period conditional on the data today. 

$$\mathbb{E}[\Xi_{t+1} | \Sigma_{1:t}] \sim \mathcal{N} \left( \Phi \mu_t, \tau_t \Phi'\Phi + \sigma^2_{\xi} \right) $$


We also need the conditional covariance between $\Xi_{t+1}$ and $\Xi_t$. 

$$\mbox{Cov}[\Xi_{t+1}, \Xi_{t} \vert \Sigma_{1:t} ] = \Phi \tau_t $$


Now, that we have those terms we are capable of deriving the conditional distribution of $\Xi_t \vert \Xi_{t+1}, \Omega_t$, and since the process is Markov in $\Xi_t$, this is simply $\Xi_t \vert \Omega_{t+1}$. 



$$\Xi_{t} \vert \Omega_{t+1} \sim \mathcal{N} \left( \mu_t + \tau_t \Phi' \left(\tau_t \Phi' \Phi + \sigma^2_{\xi} \mathbb{I}_K \right)^{-1} (\Xi_{t+1} - \Phi \mu_t ), \tau_t (\mathbb{I}_K - \tau_t \Phi' \left(\tau_t \Phi' \Phi + \sigma^2_{\xi} \mathbb{I}_K \right)^{-1} \Phi \right)$$


\section{Conditional distribution of the parameters}

The above steps give us methods for deriving the conditional distribution the states from the smoother.
However, to implement the Gibbs sampler we still need to draw the parameters as well.
Thankfully, as long as we use conjugate priors everything should be fine. 

We start by deriving the conditional posterior of $\mu$.  
We note that it only depends upon the rest of the distribution through the equation $\Sigma_t = \mu - \Xi_t + \sigma_{\eta} H_t$.  
Therefore, we only have to worry about this distribution. 
Hence the "likelihood" for $\mu$ is as follows. 

$$\prod_{t=1}^T \frac{1}{\sigma_{\eta} \sqrt{2 \pi}} \exp\left\lbrace -\frac{1}{2 \sigma^2_{\eta}} \left( \Sigma_t - \mu - \Xi_t \right)' \left( \Sigma_t - \mu - \Xi_t \right) \right\rbrace $$ 

If we choose a Gaussian prior distribution of the form $\mathcal{N}\left(\tilde{\mu}, \sigma_{\mu}\right)$, then through standard arguments we have the following posterior. 

%$$\mu \sim \mathcal{N}\left( \frac{1}{\sigma^2_{\eta} + \frac{1}{\sigma^{\mu} \right) \left(\frac{1}{T \sigma_{\eta}^2} \sum_{t=1}^T \left(\Sigma_t - \Xi_t \right) + \frac{\tilde{\mu}}{\sigma^2_{\mu}}\right), \frac{1}{T} \left( \frac{1}{\sigma^2_{\eta} + \frac{1}{\sigma^{\mu} \right)^{-1} \right)$$ 

$$\mu | \Sigma, \Xi, \sigma_{\eta}^2 \sim \mathcal{N}\left( \left(\frac{1}{\sigma^2_{\eta}} + \frac{1}{T \sigma^2_{\mu}} \right)^{-1} \left(\frac{1}{T \sigma_{\eta}^2} \sum_{t=1}^T \left(\Sigma_t - \Xi_t \right) + \frac{\tilde{\mu}}{ T \sigma^2_{\mu}}\right), \frac{1}{T} \left(\frac{1}{\sigma^2_{\eta}} + \frac{1}{T \sigma^2_{\mu}} \right)^{-1} \right)$$ 


\subsection{Posterior variance distributions}

We now derive the posterior distributions of $\sigma^2_{\eta}$ and $\sigma^2_{\xi}$. 
Again we consider the conjugate prior case. For example, $p(\sigma^2_{\eta}) = \frac{1}{(\sigma^2_{\eta})^{\tilde{dirichlet_param} - 1}} \exp\left\lbrace -\frac{\tilde{\beta}}{\sigma^2_{\eta}}\right\rbrace$.
This gives us the following standard posterior distribution. 
One does have to take into account that we are observing multiple observations in each period, and hence the correct update depends upon the total sample size, not just the number of days. 

$$\sigma^2_{\eta} | \Xi, \Sigma, \mu \sim \mbox{Inverse-Gamma}\left(\tilde{dirichlet_param} + \frac{T*K}{2}, \tilde{\beta} + \frac{1}{2} \sum_{t=1}^T (\Sigma_t - \mu - \Xi_t)'(\Sigma_t - \mu - \Xi_t)\right)$$

The posterior for $\sigma^2_{\xi}$ is essentially identical. 

$$\sigma^2_{\xi} | \Xi, \Phi \sim \mbox{Inverse-Gamma}\left(\tilde{dirichlet_param} + \frac{(T-1)*K}{2}, \tilde{\beta} + \frac{1}{2} \sum_{t=2}^T (\Xi - \Phi' \Xi_{t-1})'(\Xi_t - \Phi' \Xi_{t-1})\right)$$
 
 
The setup for $\Phi$ is the standard matrix-gaussian setup. 
We let the prior for $\Phi$ have a matrix-gaussian distribtion $p(\Phi) \sim \mathcal{N}\left(\tilde{\Phi}, \tilde{\Sigma}_{\Phi}\right)$. 
Since this is the standard conjugate normal case, it has the standard posterior. 

\begin{multline*}
\mbox{vec}(\Phi) | \sigma_{\xi}^2, \Xi, \sim \mathcal{N} \left( \Sigma_{\Phi} \left(\sigma^{-2}_{\xi} \left(\mathbb{I}_{K} \otimes (\Xi_{1:}'\Xi_{1:})\right) \mbox{vec}\left((\Xi_{1:}'\Xi_{1:})^{-1} \Xi_{1:}' \Xi_{0:T-1}\right) +  \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_K \mbox{vec}\tilde{\Phi} \right), \right. \\ \left.
 \Sigma_{\Phi} = \left( \sigma^{-2}_{\xi} \mathbb{I}_K \otimes \left( \Xi_{1:}' \Xi_{1:}\right) + \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_{K^2} \right)^{-1} \right)
\end{multline*}


We can now use properties of the Kronecker product, and derive the following. 
In this case, we assume that $\tilde{\Sigma}_{\Phi} = \tilde{\sigma}_{\Phi} \mathbb{I}_K \otimes \mathbb{I}_{T-1}$. 

\begin{multline*}
\mbox{vec}(\Phi) | \sigma_{\xi}^{2}, \Xi \sim \mathcal{N} \left(\mbox{vec} \left(\left(\sigma_{\xi}^{-2} \Xi_{1:}'\Xi_{0:T-1} + \tilde{\sigma}_{\Phi}^{-2} \tilde{\Phi}\right) \left( \sigma_{\xi}^{-2} (\Xi_{1:}' \Xi_{1:}) + \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_{K}\right)^{-1}\right),\right. \\ \left.
\left(\left( \sigma_{\xi}^{-2} (\Xi_{1:}' \Xi_{1:}) + \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_{K}\right)^{-1} \otimes \mathbb{I}_K \right)\right)
\end{multline*}

In order to speed up the computation you can take advantage of the block independence from the form above. 
In other words. 

\begin{multline*}
\mbox{col}_k(\Phi) | \sigma_{\xi}^{2}, \Xi \sim \mathcal{N}\left(  \mbox{col}_k \left(\left(\sigma_{\xi}^{-2} \Xi_{1:}'\Xi_{0:T-1} + \tilde{\sigma}_{\Phi}^{-2} \tilde{\Phi}\right) \left( \sigma_{\xi}^{-2} \Xi_{0:T-1}' \Xi_{1:} + \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_{K}\right)^{-1}\right), \right. \\  \left. \left.
\left( \sigma_{\xi}^{-2} \Xi_{1:}' \Xi_{0:T-1} + \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_{K}\right)^{-1} \right) \right)
\end{multline*}


Equivalently, we can place this in the matrix-normal notation. 
Let $\Sigma_{post} = \left( \sigma_{\xi}^{-2} \Xi_{1:}' \Xi_{0:T-1} + \tilde{\sigma}_{\Phi}^{-2} \mathbb{I}_{K}\right)^{-1}$, 
$M_{post} = \left(\sigma_{\xi}^{-2} \Xi_{1:}'\Xi_{0:T-1} + \tilde{\sigma}_{\Phi}^{-2} \tilde{\Phi}\right)$

$$\Phi \vert \sigma_{\xi}^2, \Xi \sim \mbox{Matrix-Norma} \left(M_{post} \Sigma_{post}, \mathbb{I}_K, \Sigma_{post}\right)$$. 

I only consider diagonalizable matrices because the set of non-diagonalizable ones has lebesque measure zero, and hence since the gaussian and lebesque measures are mutually absolutely continuous, it has zero measure under any matrix-valued map as well. 

Hence, we can use the eigendecomposition to draw from the relevant posterior. 
In other words, we consider $\Phi = Q'\Lambda Q^{-1}$. 

Then, since it is simply a linear transformation, we have the following, where we simplify using the associative properties of matrix multiplication and kronecker products. 

$$\Lambda \sim \mbox{Matrix-Normal}\left(Q^{-1} M \Sigma_{post} Q, (Q^{-1})Q^{-1}, Q' \Sigma_{post}Q  \right)$$

We can do the linear transformation trick again, this time using selection vectors, and derive the following result
This implies that $\lambda_i \sim \mathcal{N}\left(\left(Q^{-1} M \Sigma_{post} Q\right)_{ii}, Q^{-2}_{ii} (Q' \Sigma_{post} Q)_{ii} \right)$.  

(Although, the above derivation is correct in some respects, we have a major issue in that the eigenvalues might be complex. However, a normal random variable only produces real-valued variate.) 

\subsection{Stationary Matrices Drawing Process}
To draw from the distribution over stationary matrices, first take a draw from the conditional posterior. 
Then eigendecompose this matrix, and take its eigenvectors $Q$. 
These are from the correct distribution because by the derivation above, the eigenvectors and eigenvalues are jointly complex normal, and hence dropping the eigenvalues is akin to marginalizing them out, which is what you want to do to get the correct distribution over the eigenvectors. 

Now, we use a Metropolis Hastings step to draw from the eigenvalues.  
If the eigenvalue has a modulus greater than 1, we first draw the real part $r$ uniformly from 1 to -1; we then draw the complex part uniformly from $(-\sqrt{1 - r^2}, \sqrt{1 - r^2}$.
If it is not, draw from the induced normal distribution given above, truncated so that the eigenvalues have modulus less than 1. 
The eigenvalues are conditionally independent, so using different proposal distributions should not in any way affect anything.  
In addition the proposal distribution in the first part is uniform, and in the second part it is a random-walk metropolis-hastings algorithm. 
As a result, we do not need to weight the metropolis-hastings $\concentration$ by the proposal probabilities in either case.

\textit{This should work reasonably well in practice, and I think is theoretically valid.}




\end{document}


