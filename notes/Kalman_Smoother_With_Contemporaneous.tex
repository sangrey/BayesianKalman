\documentclass[11pt]{amsart}
\usepackage{graphicx}
\usepackage{amsmath, amsfonts, amsthm, amssymb, latexsym, mathtools}
\usepackage{mathrsfs}
\usepackage{etex}
\usepackage{etoolbox}
\usepackage{kvoptions}
\usepackage{logreq}
\usepackage{csquotes}
\usepackage[american]{babel}
\usepackage[doublespacing]{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[backend=biber, authordate]{biblatex-chicago}
\usepackage{placeins}


\begin{document}


\section{Introduction}
 
 In the notes below we derive a Kalman Filter and Kalman Smoother in the case where the contemporaneous shocks are independent and identically-distributed.  If one wanted to it in more generality, that would not be too difficult. You can follow the steps below as a guide.
 
The key to the derivation is to use properties and conditional normality and exploit the recursive structure of the problem. 


The model we assume has the following structure. 

\begin{align*}
V_t = M + Z \Xi_t + \Sigma_{\eta}^{1/2} H_t \\
\Xi_t = \Phi \Xi_{t-1} + \Sigma_{\xi}^{1/2} U_t \\
\end{align*}



\section{Kalman Filter}


We use $\Omega_t$ to refer to the information set at time $t$. 
This is the typical Kalman filter, and can be estimated using standard methods. 

Since this is a Kalman filtering model, the updating steps are standard. Let $\alpha_{t \vert l} = \mathbb{E}\left[\Xi_t \middle\vert \Omega_l \right]$, $P_{t \vert l} = \mbox{Var}\left[\Xi_t \middle\vert \Omega_{l} \right]$. 

\begin{align*}
\alpha_{t \vert t-1} &= \Phi \Xi_{t-1} \\
P_{t \vert t-1} &= \Phi P_{t-1 \vert t-1} \Phi' + \Sigma_{\xi} \\
\alpha_{t \vert t} &= \alpha_{t \vert t-1} + P_{t \vert t-1} Z' \left(Z P_{t\vert t-1} Z' + \Sigma_{\eta}\right)^{-1} \left(V_t - Z \alpha_{t \vert t-1} - M\right) \\
P_{t \vert t} &= P_{t \vert t-1} -  P_{t \vert t-1} Z' \left(Z P_{t\vert t-1} Z' + \Sigma_{\eta}\right)^{-1} Z P_{t \vert t-1} 
\end{align*}


\section{Kalman Smoother}

If we simply want to estimate the factor and aren't worried about an online algorithm, then we would want to use all the information including values in the future. 

Let $M_t$ be $\mathbb{E}[V_t | \Omega t]$, and $\tau_t$ be the coefficient in front of the identity in $\mbox{Var}[V_t | \omega_t ]$. 
In other words, the values in the expression right above. 

Using that notation we can derive the distribution of state in the period conditional on the data today.
It is standard, and so using the $P_t$ to refer to the conditional variance delivered by the Kalman filter, we have. 

$$\mbox{Var}\left[\Xi_t \vert \Omega_t, \Xi_{t+1}\right] = P_t - P_t \Phi' \left( \Phi P_t \Phi' + \Sigma_{\xi}\right)^{-1} \Phi P_t$$

$$\mathbb{E}\left[\Xi_t \vert \Omega_t, \Xi_{t+1} \right] = \mathbb{E}\left[\Xi_t \vert \Omega_t \right] + P_t \Phi' \left(\Phi' P_t \Phi + \Sigma_{\xi} \right)^{-1} \left(\Xi_{t+1} - \Phi \mathbb{E}\left[\Xi \vert \Omega_t \right]\right) $$


This gives us the conditional distribution of $\Xi_t$.

$$\Xi_t \vert \Omega_T \sim \mathcal{N}\left(\mathbb{E}\left[\Xi_t \vert \Omega_t, \Xi_{t+1} \right], \mbox{Var}\left[\Xi_t \vert \Omega_t, \Xi_{t+1}\right]\right)$$

\section{Conditional distribution of the parameters}

The above steps give us methods for deriving the conditional distribution the states from the smoother.
However, to implement the Gibbs sampler we still need to draw the parameters as well.
Thankfully, as long as we use conjugate priors everything should be fine. 

We start by deriving the conditional posterior of $M$.  
We note that it only depends upon the rest of the distribution through the equation $V_t = M - \Xi_t + \sigma_{\eta} H_t$.  
Therefore, we only have to worry about this distribution. 
Hence the ``likelihood" for $M$ is as follows. 

$$\prod_{t=1}^T \frac{1}{2}\vert \Sigma_{\eta}\vert^{-1/2}   \exp\left\lbrace -\frac{1}{2} \left( V_t - M - Z \Xi_t \right)'\Sigma_{\eta}^{-1} \left( V_t - M - Z \Xi_t \right) \right\rbrace $$ 

If we choose a Gaussian prior distribution of the form $\mathcal{N}\left(\tilde{M}, \sigma_{M}\right)$, then through standard arguments we have the following posterior. 

$$M | V, \Xi, \Sigma_{\eta} \sim \mathcal{N} \left( \left(\Sigma_{\eta}^{-1} + \Sigma_M^{-1}\right)^{-1} \left(\Sigma_v^{-1} \frac{1}{T} \sum_{t=1}^T (V_t - Z \Xi_t) + \frac{1}{T} \Sigma_M^{-1} M_M \right), \frac{1}{T} \left(\Sigma_{\eta}^{-1} + \Sigma_M^{-1}\right)^{-1}\right)$$

\subsection{Posterior variance distributions}

We now derive the posterior distributions of $\Sigma^2_{\eta}$ and $\Sigma^2_{\xi}$. 
Again we consider the conjugate prior case, that is we use inverse-wishart priors.
This gives us the following standard posterior distribution. 

$$\Sigma_{\eta} | \Xi, \Sigma, M \sim \mbox{Inverse-Gamma}\left(\nu_{\Sigma_{\eta}} + T, \left( \Psi_{\Sigma_\eta} + \sum_{t=1}^T (V_t - M - Z \Xi_t)'(V_t - M - Z \Xi_t)\right)\right)$$

The posterior for $\sigma^2_{\xi}$ is essentially identical. 

$$\Sigma_{\eta} | \Xi, \Sigma, M \sim \mbox{Inverse-Wishart}\left(\nu_{\Sigma_{\xi}} + (T-1), \left( \Psi_{\Sigma_\xi} + \sum_{t=1}^T (\Xi_t - \Phi' \Xi_{t-1})'(\Xi_t - \Phi' \Xi_{t-1}) \right)\right)$$
 

\subsection{Posterior Transition Matrix distribution}
 
The setup for $\Phi$ is the standard matrix-gaussian setup. 
We let the prior for $\Phi$ have a matrix-gaussian distribution $p(\Phi) \sim \mathcal{N}\left(M_{\phi}, \mathbb{I}_K, V_{\Phi} \right)$. 
Since this is the standard conjugate normal case, it has the standard posterior. 

First, we define some notation. 

\begin{align*}
\hat{\Phi} &= (\Xi_{1:T-1}'\Xi_{1:T-1})^{-1} \Xi_{1:T-1} \Xi_{2:T}\\
V_1 &= \left[\Sigma_{\xi}^{-1} \otimes (\Xi_{1:T-1}'\Xi_{1:T-1}) + \left(V_{\Phi}^{-1} \otimes \mathbb{I}_K \right)\right]^{-1}
\end{align*}

$$\mbox{vec}\left(\Phi\right) | \Xi, \Sigma_{\xi}, V_{\Phi}, M_{\Phi} \sim \mathcal{N} \left(
V_1 \left[ \left( \Sigma_{\xi}^{-1} \otimes (\Xi_{1:T-1}'\Xi_{1:T-1}) \right)\mbox{vec}\left(\hat{\Phi}\right) + \left(V_{\Phi}^{-1} \otimes \mathbb{I}_K \right) \mbox{vec}\left(M_{\Phi}\right) \right]\right)$$


\subsection{Posterior Of Mean Deviation}

The conditional posterior of the mean deviation is standard for these sorts of models.  It is simply a multivariate normal with a conjugate prior, which leads to the following posterior. 

\begin{equation*}
M_v \sim \mathcal{N}\left(\left(\Sigma_v^{-1} + \Sigma_M^{-1} \right)^{-1} \left( \Sigma_v^{-1} \frac{1}{T} \sum_{t=1}^T \left(V_t - Z' \Xi_t\right) + \frac{1}{T} \Sigma_m^{-1} M_m \right), \frac{1}{T}  \left(\Sigma_v^{-1} + \Sigma_M^{-1} \right)^{-1}\right)
\end{equation*}





\end{document}


